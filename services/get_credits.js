'use strict';
const mongoose = require('mongoose');
const user = require('../models/usermodel');
const customerUser = require('../models/customermodel.js');
const config = require('../config/config.json');

exports.get = (req,res,callback)=>{
	console.log(req.body.id);
	user.findOne({id:req.body.id},'purchasedPoints promotionalPoints totalPoints',(err,user)=>{
		if(err){
			console.log(err);
			callback(err);
		}
		else{
		callback({'purchasedPoints':user.purchasedPoints,
			'promotionalPoints':user.promotionalPoints,
			'totalPoints':user.totalPoints});
		}
	})
}

exports.getCustomer = (req,res,callback)=>{
	console.log(req.body.id);
	customerUser.findOne({id:req.body.id},'purchasedPoints promotionalPoints totalPoints',(err,user)=>{
		if(err){
			console.log(err);
			callback(err);
		}
		else{
		callback({'purchasedPoints':user.purchasedPoints,
			'promotionalPoints':user.promotionalPoints,
			'totalPoints':user.totalPoints});
		}
	})
}

exports.check = (req,res,callback)=>{
	user.findOne({id:req.body.id},'totalPoints',(err,user)=>{
		if(err)
			console.log(err)
		else
		{
			if(user.totalPoints>=config.release_offer_cost)
				callback({'res':true});
			else
				callback({'res':false});

		}
	});
}

exports.checkCustomer = (req,res,callback)=>{
	customerUser.findOne({id:req.body.id},'totalPoints',(err,user)=>{
		if(err)
			console.log(err)
		else
		{
			if(user.totalPoints>=config.release_offer_cost)
				callback({'res':true});
			else
				callback({'res':false});

		}
	});
}