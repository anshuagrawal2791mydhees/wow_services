'use strict';
const mongoose = require ('mongoose');
const user = require('../models/usermodel');
const customerUser = require('../models/customermodel.js');

exports.add = (req,res,callback)=>{

	user.findOne({id:req.body.customerId},(err,user)=>{
		console.log(err);
		user.purchasedPoints+=parseInt(req.body.purchased);
		user.promotionalPoints+= parseInt(req.body.promotional);
		user.totalPoints = user.purchasedPoints+user.promotionalPoints;	//@Anshu->logic present in schema already.
		user.save((err)=>{
			if(err)
				callback({'res':false,'error':err});
			else
			callback({'res':true});
		});
	});
}

exports.addCustomer = (req,res,callback)=>{

	customerUser.findOne({id:req.body.customerId},(err,user)=>{
		console.log(err);
		user.purchasedPoints+=parseInt(req.body.purchased);
		user.promotionalPoints+= parseInt(req.body.promotional);
		user.totalPoints = user.purchasedPoints+user.promotionalPoints;	//@Anshu->logic present in schema already.
		user.save((err)=>{
			if(err)
				callback({'res':false,'error':err});
			else
			callback({'res':true});
		});
	});
}

