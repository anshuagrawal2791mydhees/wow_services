'use strict';

const mongoose = require('mongoose'); 
const hotel = require('../models/hotelmodel.js');
const room = require('../models/roommodel.js');
const multer  = require('multer');
const id_generator = require('../utils/create_id_util');
// var uploads = multer({ dest: 'uploads/' });
const upload_s3  = require('../services/upload_s3');

const upload_grid  = require('../services/upload_grid');

const fs = require ('fs');
const path = require('path')
var Path = path.join(__dirname, '../uploads');

const shortid = require('shortid');
 	 shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

const deleteFolderRecursive = require('../utils/delete_directory_content');

exports.register = (req,res,callback) =>{  


			// let temp =rand(160, 36); 
			// let newpass = temp + password; 
			// let hashed_password = crypto.createHash('sha512').update(newpass).digest("hex"); 

			// let selected_services = JSON.parse(services);
			let current_Date = new Date();
			//console.log(city);
			let facilitesJSON = JSON.parse(req.body.facilities);
			
			let newhotel = new hotel({ 
				ownerId:        req.body.ownerId, 
				hotelName:      req.body.hotelName, 
				hotelType:      req.body.hotelType, 	
				noOfRooms:      req.body.noOfRooms,    	
				phoneNumber:    req.body.phoneNumber,	
				mobileNumber:   req.body.mobileNumber,	
				address_line1:  req.body.address_line1,	
				address_line2:  req.body.address_line2,
				address: 		req.body.address,
				location: 		[req.body.longitude,req.body.latitude],	
				city:           req.body.city,		
				pin:            req.body.pin,
				facilities: {
					hasWifi:            facilitesJSON['hasWifi'],
					hasDining:          facilitesJSON['hasDining'],
					hasTv:              facilitesJSON['hasTv'],
					hasGym:             facilitesJSON['hasGym'],
					hasBusinessService: facilitesJSON['hasBusinessService'],
					hasCoffeeShop:      facilitesJSON['hasCoffeeShop'],
					hasFrontDesk:       facilitesJSON['hasFrontDesk'],
					hasSpa:             facilitesJSON['hasSpa'],
					hasLaundry:         facilitesJSON['hasLaundry'],
					hasOutDoorgames:    facilitesJSON['hasOutDoorgames'],
					hasParking:         facilitesJSON['hasParking'],
					hasRoomService:     facilitesJSON['hasRoomService'],
					hasSwimming:        facilitesJSON['hasSwimming'],
					hasTravelAssistance:facilitesJSON['hasTravelAssistance']             
				},

				starHotel:     req.body.starHotel,
				//policies:      req.body.policies,
				isVerified:    false,
				createdBy:     req.body.email,
				createdAt:     current_Date,
				modifiedBy:    req.body.email,
				modifiedAt:    current_Date,
				status:        true,
				lastActivity:  current_Date
				
			});
			id_generator.create_id("HT",req.body.city,req.body.pin,(found)=>{
				newhotel.hotelId = found['id'];
				//console.log(found['id']);
			}); 
			let policiesJSON = JSON.parse(req.body.policies);
			policiesJSON.forEach((item)=>{
				newhotel.policies.push(item);
			});

			function final(count_grid,count_s3){
				if(count_s3 <=0 && count_grid<=0)
				{
					newhotel.save((err)=> { 
						console.log(err);
						callback({'res':true,"hotelId":newhotel.hotelId,"noOfRooms":newhotel.noOfRooms,
						'hotelName':newhotel.hotelName,
						'imagesS3': newhotel.imagesS3,
						'address_line1':newhotel.address_line1,
						'address_line2':newhotel.address_line2,
						'city': newhotel.city,
						'address':newhotel.address,
						'facilities':newhotel.facilities,
						'policies': newhotel.policies,
						'phoneNumber': newhotel.phoneNumber,
						'mobileNumber': newhotel.mobileNumber,
						'hotelType':newhotel.hotelType,
						'starHotel':newhotel.starHotel});
						deleteFolderRecursive.delete(Path,(found)=>{
							console.log(found);
						});
						
					});
				}
			};
			
			  
						 //console.log(req.files[0].originalname);
						 let count_grid = req.files.length;
						 let count_s3 = req.files.length;
						 if(count_grid>0){
						 // req.files.forEach((file)=>{
						 	for(var i=0;i<req.files.length;i++){
						 	var file_id = newhotel.hotelId + "-com-" + shortid.generate();
						 	newhotel.imagesGridFS.name.push(file_id);
						 	newhotel.imagesGridFS.description.push(JSON.parse(req.body.description)[i]);
						 	newhotel.imagesS3.name.push(file_id);
						 	newhotel.imagesS3.description.push(JSON.parse(req.body.description)[i]);
						 	upload_grid.upload(req.files[i],file_id,()=>{
						 		console.log("count_grid"+count_grid);
						 		
						 		count_grid--;
						 		if(count_grid<=0){
						 			console.log("grid_done");
						 			final(count_grid,count_s3);
						 		}
						 	});	
						 
						 
						 	upload_s3.upload(req.files[i],file_id,()=>{
						 		console.log("count_s3"+count_s3)
						 		count_s3--;
						 		if(count_s3<=0){
						 			console.log('s3 done');
						 			final(count_grid,count_s3);
						 		}

						 	});
						 }

						// });
						}
						else{
						 newhotel.save((err)=> {
						  
								callback({'res':true,"hotelId":newhotel.hotelId,"noOfRooms":newhotel.noOfRooms,
						'hotelName':newhotel.hotelName,
						'imagesS3': newhotel.imagesS3,
						'address_line1':newhotel.address_line1,
						'address_line2':newhotel.address_line2,
						'address':newhotel.address,
						'facilities':newhotel.facilities,
						'policies': newhotel.policies,
						'phoneNumber': newhotel.phoneNumber,
						'mobileNumber': newhotel.mobileNumber,
						'hotelType':newhotel.hotelType,
						'city':newhotel.city,
						'starHotel':newhotel.starHotel});
							});
						}

}
exports.update = (req,res,callback) => {

	if(!req.body.hotelId)
		callback({'response':"Invalid HotelId!", 'res':false});
	else {
		let current_Date = new Date();
		let updateHotel = {};
		if(req.body.ownerId)	{ updateHotel.ownerId = req.body.ownerId}
		if(req.body.hotelName)	{ updateHotel.hotelName = req.body.hotelName }
		if(req.body.hotelType)	{ updateHotel.hotelType = req.body.hotelType }
		if(req.body.noOfRooms)	{ updateHotel.noOfRooms = req.body.noOfRooms }
		if(req.body.phoneNumber)	{ updateHotel.phoneNumber = req.body.phoneNumber }
		if(req.body.mobileNumber)	{ updateHotel.mobileNumber = req.body.mobileNumber }
		if(req.body.address_line1)	{ updateHotel.address_line1 = req.body.address_line1 }
		if(req.body.address_line2)	{ updateHotel.address_line2 = req.body.address_line2 }
		if(req.body.address)	{ updateHotel.address = req.body.address }
		if(req.body.location)	{ updateHotel.location = [req.body.longitude,req.body.latitude] }
		if(req.body.pin)	{ updateHotel.pin = req.body.pin }
		if(req.body.city)	{ updateHotel.city = req.body.city } 
	
		//updates facilities instead of overwriting
		if(req.body.facilities)	{
			let facilitiesJSON = JSON.parse(req.body.facilities);
			let facilitiesKey = ['hasWifi', 'hasDining', 'hasTv', 'hasGym', 'hasBusinessService', 'hasCoffeeShop', 'hasFrontDesk', 'hasSpa', 'hasLaundry', 'hasOutDoorgames', 'hasParking', 'hasRoomService','hasSwimming', 'hasTravelAssistance'];
			facilitiesKey.forEach((item)=>	{
				let key = "facilities."+item;
				if(facilitiesJSON.hasOwnProperty(item)){
					updateHotel[key] = facilitiesJSON[item];
				}
			});
		}

		if(req.body.starHotel)	{ updateHotel.starHotel = req.body.starHotel } 
		if(req.body.isVerified)	{ updateHotel.isVerified = req.body.isVerified } 
		if(req.body.policies)	{
			let policiesJSON = JSON.parse(req.body.policies);
			updateHotel.policies = [];
			policiesJSON.forEach((item)=>{
				updateHotel.policies.push(item);
			});
		}

		updateHotel.modifiedBy = req.body.email		//email always required.
		updateHotel.modifiedAt = current_Date
		if(req.body.status)	{ updateHotel.status = req.body.status } 
		
		//Updating with updateHotel

		updateHotel.imagesS3 = {};
		updateHotel.imagesS3.name = [];
		updateHotel.imagesS3.description =[];
		//updateHotel.imagesGridFS.name = [];


		console.log(req.files.length);
						//let count_grid = req.files.length;
						 let count_s3 = req.files.length;
						 final(count_s3);
						 if(count_s3>0){
						 // req.files.forEach((file)=>{
						 	for(var i=0;i<req.files.length;i++){
						 	var file_id = req.body.hotelId + "-com-" + shortid.generate();
						 	// newhotel.imagesGridFS.name.push(file_id);
						 	// newhotel.imagesGridFS.description.push(JSON.parse(req.body.description)[i]);
						 	updateHotel.imagesS3.name.push(file_id);
						 	updateHotel.imagesS3.description.push(JSON.parse(req.body.description)[i]);
						 	// upload_grid.upload(req.files[i],file_id,()=>{
						 	// 	console.log("count_grid"+count_grid);
						 		
						 	// 	count_grid--;
						 	// 	if(count_grid<=0){
						 	// 		console.log("grid_done");
						 	// 		final(count_grid,count_s3);
						 	// 	}
						 	// });	
						 
						 
						 	upload_s3.upload(req.files[i],file_id,()=>{
						 		console.log("count_s3"+count_s3)
						 		count_s3--;
						 		if(count_s3<=0){
						 			console.log('s3 done');
						 			final(count_s3);
						 		}

						 	});
						 }

						// });
						}


		function final(count_s3){
				if(count_s3 <=0)
				{
					deleteFolderRecursive.delete(Path,(found)=>{
							console.log(found);
						});
					hotel.update({hotelId: req.body.hotelId},{
			$set : updateHotel
		},(err, numberAffected, rawResponse)=> {
			if(err)
				callback({'response':"Error updating hotel details!",'res':false});
			else{
				console.log(numberAffected);
				if(numberAffected.n == 0)
					callback({'response':"Invalid HotelId!", 'res':false});
				else
				{
					room.update({hotelId: req.body.hotelId},{
						$set : {
								'hotelName' : updateHotel.hotelName
							}
						},{ multi: true }, (err2, numberAffected2, rawResponse2)=> {
							if(err2)
								callback({'response':"Error updating hotel details for rooms. Hotel details updated!",'res':false});
							else {
								hotel.findOne({hotelId: req.body.hotelId},(err,newhotel)=>{
								callback({'response':"Successfully updated hotel details!",'res':true,rawResponse,"hotelId":newhotel.hotelId,"noOfRooms":newhotel.noOfRooms,
									'hotelName':newhotel.hotelName,
									'imagesS3': newhotel.imagesS3,
									'address_line1':newhotel.address_line1,
									'address_line2':newhotel.address_line2,
									'address':newhotel.address,
									'facilities':newhotel.facilities,
									'policies': newhotel.policies,
									'phoneNumber': newhotel.phoneNumber,
									'mobileNumber': newhotel.mobileNumber,
									'hotelType':newhotel.hotelType,
									'city':newhotel.city,
									'starHotel':newhotel.starHotel,'imagesS3':newhotel.imagesS3});

								}); //hotel details send
							}
						}); //room update end
				}
			}

		});//hotel update end
				}
			};

		
	}//else end
}

exports.getList = (req, res, callback) => {
	let params = {};
	params.city = req.body.city;
	params.isDeleted = false;
	/*
	params.starHotel = 0; 
	if(substr(req.body.starHotel, -1, -1) == "+")
	{

	}
	else if(substr(req.body.starHotel, -1, -1) == "-")
	{

	}
	else
	{

	}
	*/
	hotel.find({ city : req.body.city, isDeleted : false}, 'hotelId hotelName location address_line1 address_line2 address', (err, hotels)=>{
		if(err) {
			callback({'response':"Error getting hotels.", 'res':false});
		}
		else {
			callback({ 'res' : true,
				'hotels' : hotels
			});
		}
	});
}
exports.get = (req,res,callback)=>{
	hotel.findOne({hotelId: req.body.hotelId, isDeleted : false},(err,hotel)=>{
		if(err)
			console.log(err);
		else{
			callback({'res':true,'hotel':hotel});
		}
	});
}
exports.getAddress = (req,res,callback)=>{
	hotel.findOne({hotelId: req.body.hotelId, isDeleted : false},'address_line1 address_line2 address',(err,hotel)=>{
		if(err)
			console.log(err);
		else{
			callback({'res':true,'address':hotel.address,'address_line1': hotel.address_line1,'address_line2': hotel.address_line2});
		}
	});
}

exports.deleteHotel = (req,res,callback) => {
	if(!req.body.hotelId)
		callback({'response':"Invalid HotelId!", 'res':false});
	else {
		let current_Date = new Date();
		let updateHotel = {};
		
		updateHotel.isDeleted = true;
		updateHotel.modifiedBy = req.body.email;		//email required.
		updateHotel.modifiedAt = current_Date;
		
		//Deleting by setting isDeleted true
		hotel.update({hotelId: req.body.hotelId, "isDeleted" : false},{
			$set : updateHotel
		},(err, numberAffected, rawResponse)=> {
			if(err)
				callback({'response':"Error deleting hotel!",'res':false});
			else{
				console.log(numberAffected);
				if(numberAffected.n == 0)
					callback({'response':"Invalid HotelId!", 'res':false});
				else{
					room.update({ hotelId: req.body.hotelId }, {
						$set : { "isDeleted" : true }
					}, (err2, numberAffected2, rawResponse2)=> {
						if(err){
							console.log("Error deleting rooms for hotelId "+ req.body.hotelId);
						}
						else{
							console.log("Deleted rooms for hotelId "+ req.body.hotelId);
						}
					});
					callback({'response':"Successfully deleted hotel details!",'res':true});
				}
			}
		});
	}
}