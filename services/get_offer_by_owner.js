'use strict';
const offers = require('../models/offermodel');


exports.get_offer_by_owner = (req,res,callback)=>{
		
		//also edit to fetch current offers and other options
		//to be discussed
		//edit to fetch only undeleted offers
		offers.find({ownerId: req.body.ownerId}, (error, offers)=> {
			if(error){
				console.log("Error getting offers: "+ req.body.ownerId);
				callback({'res' : false, 'response': 'Error getting offers.'});
			}
			else{
				console.log(offers);
				callback({'res' : true,
					'ownerId'	: req.body.ownerId,
					'offers'	: offers
				});
			}

		});
		
}