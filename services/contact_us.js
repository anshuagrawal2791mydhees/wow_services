'use strict';

const mongoose = require('mongoose'); 
const contactUsModel = require('../models/contactusmodel');
const shortid = require('shortid');
 	 shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

exports.create = (req, res, callback) => {
	let current_Date = new Date();
	if(!(req.body.customerId || req.body.ownerId)) {
		callback({ 'res' :  false, 'response' : "No customer Id specified!"});
	}
	else{
		let newContactUs = new contactUsModel({
			messageId 	: 	shortid.generate(),
			customerId 	: 	req.body.customerId,
			customerName: 	req.body.name,
			email 		: 	req.body.email,
			ownerId 	: 	req.body.ownerId,
			message 	: 	req.body.message,
			type 		: 	1,
			status 		: 	1
		});
		newContactUs.save((err)=>{
			if(err) {
				callback({'res' : false, 'response' : "Error saving message!"});
			}
			else {
				callback({'res' : true, 'response' : 'Message successfully received'});
			}
		});
	}
}

exports.get = (req, res, callback) => {
	contactUsModel.find({'status' : 1}, (error, messages)=>{
		if(error){
			callback({'res' : false, 'response' : "Error getting messages!"});
		}
		else {
			callback({'res' : true, 'messages' : messages});
		}
	});
}

exports.create_feedback = (req, res, callback) => {
	let current_Date = new Date();
	if(!(req.body.customerId || req.body.ownerId)) {
		callback({ 'res' :  false, 'response' : "No customer Id specified!"});
	}
	else{
		let newContactUs = new contactUsModel({
			messageId 	: 	shortid.generate(),
			customerId 	: 	req.body.customerId,
			customerName: 	req.body.name,
			email 		: 	req.body.email,
			ownerId 	: 	req.body.ownerId,
			message 	: 	req.body.message,
			type 		: 	2,
			status 		: 	1
		});
		newContactUs.save((err)=>{
			if(err) {
				callback({'res' : false, 'response' : "Error saving message!"});
			}
			else {
				callback({'res' : true, 'response' : 'Message successfully received'});
			}
		});
	}
}

exports.get_feedback = (req, res, callback) => {
	if(!(req.body.customerId || req.body.ownerId || req.body.messageId)){
		callback({'res' : false, 'response' : "No customerId specified!"});
	}
	else{
		let params = {};
		params.type = 2;
		if(req.body.customerId)	params.customerId = req.body.customerId;
		if(req.body.ownerId)	params.ownerId = req.body.ownerId;
		if(req.body.messageId) 	params.messageId = req.body.messageId;
		
		contactUsModel.find(params, (error, messages)=>{
			if(error){
				callback({'res' : false, 'response' : "Error getting messages!"});
			}
			else {
				callback({'res' : true, 'messages' : messages});
			}
		});
	}
}

exports.reply_feedback = (req, res, callback) => {
	if(!req.body.messageId){
		callback({'res' : false, 'response' : "No messageId specified!"});
	}
	else{
		contactUsModel.find({"messageId" : req.body.messageId}, (error, message)=>{
			if(error || message.length == 0){
				callback({'res' : false, 'response' : "Error getting messages!"});
			}
			else {
				message = message[0];
				message.reply = req.body.reply;
				message.replyTime = new Date();
				message.status = 2;
				message.save((err)=>{
					if(err){
						callback({'res' : false, 'response' : "Error replying to message!"});
					}
					else{
						callback({'res' : true, 'message' : message});
					}
				});
				
			}
		});
	}
}

