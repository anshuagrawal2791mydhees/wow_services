 var _ = require('underscore'),
 	AWS = require('aws-sdk'),
 	fs = require('fs'),
 	path = require('path'),
 	flow = require('flow');
 	const shortid = require('shortid');
 	 shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');


 configPath = path.join(__dirname, '../config', "config.json");

 AWS.config.loadFromPath(configPath);

 exports.upload = function(file_to_upload,filename,callback) {
 	
 	var s3 = new AWS.S3(),
 		file = file_to_upload,
 		result = {
 			error: 0,
 			uploaded: []
 		};

 	flow.exec(
 		function() { // Read temp File
 			fs.readFile(file.path, this);
 		},
 		function(err, data) { // Upload file to S3
 			s3.putObject({
 				Bucket: 'wowimagesupload', //Bucket Name
 				Key: filename+".jpg", //Upload File Name, Default the original name
 				Body: data,
 				ContentType: file.mimetype
 			}, this);
 		},
 		function(err, data) { //Upload Callback
 			if (err) {
 				console.error('Error : ' + err);
 				result.error++;
 				console.log(err);
 			}
 			console.log("s3 ok")
 			result.uploaded.push(data.ETag);
 			this();
 		},
 		function() {
 			// res.render("result", {
 			// 	title: "Upload Result",
 			// 	message: result.error > 0 ? "Something is wroing, Check your server log" : "Success!!",
 			// 	entitiyIDs: result.uploaded
 			// });
 			// res.json({'result': result.error,
 			// 	'entitiyIDs':result.uploaded

 			// });
 			console.log(result.uploaded);
 			callback();
 		});
 
 };
