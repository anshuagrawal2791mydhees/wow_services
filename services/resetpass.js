'use strict';

const crypto = require('crypto'); 
const rand = require('csprng'); 
const mongoose = require('mongoose'); 
const nodemailer = require('nodemailer'); 
const user = require('../models/usermodel.js');
const xoauth2 = require('xoauth2');
const configs = require('../config/config')

let smtpTransport = nodemailer.createTransport({
      host : "send.one.com",
    secureConnection : false,
    port: 465,
    auth : {
        user : configs.email_username,
        pass : configs.email_password
    }
 });


exports.respass_init = (email,callback)=> {  

	let temp =rand(24, 24); 
	user.find({email: email},(err,users)=>{  

		if(users.length != 0){   
				user.update({email: email}, {
   				 temp_str: temp
					},(err, numberAffected, rawResponse)=> {
   					console.log(temp);

   					var mailOptions = {     
					from: "MyDhees  <noreply@mydhees.com>",     
					to: email,     
					subject: "Reset Password ",     
					text: "Hello "+users[0].name+".  Code to reset your Password is "+temp+"",  
					}
				smtpTransport.sendMail(mailOptions,(error, response)=>{     
					if(error){  
						 console.log(error);
						callback({'response':"Error While Resetting password. Try Again !",'res':false});      

					}else{  

						callback({'response':"Check your Email and enter the verification code to reset your Password.",'res':true});      

					} 

				}); 
					});  

			
		}else{  

			callback({'response':"Email Does not Exists.",'res':false});  

		} 
	}); 
} 

exports.respass_chg = (email,code,npass,callback) =>{   

	user.find({email: email},(err,users)=>{  

		if(users.length != 0){  

			let temp = users[0].temp_str; 
			// console.log(temp);
			let temp1 =rand(160, 36); 
			let newpass1 = temp1 + npass; 
			let hashed_password = crypto.createHash('sha512').update(newpass1).digest("hex");  
			
			if(temp == code){ 

					user.findOne({ email: email },(err, doc)=>{ 
						// console.log("new pass: "+ npass);
						// console.log("new salt: "+ temp1);
						// console.log("new hashed_password: "+hashed_password);  
						doc.hashed_password= hashed_password;
						doc.salt = temp1;   
						doc.temp_str = "";
						doc.modified_at = new Date();   
						doc.save();  

						callback({'response':"Password Sucessfully Changed",'res':true});  

					});
				}
				else{  

					callback({'response':"Code does not match. Try Again !",'res':false});  

				} 
			}else{  

				callback({'response':"Error",'res':true});  

			} 
		}); 
}  


























