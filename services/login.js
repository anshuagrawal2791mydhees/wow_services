'use strict';
const crypto = require('crypto'); 
const rand = require('csprng'); 
const mongoose = require('mongoose'); 
// var gravatar = require('gravatar'); 
const user = require('../models/usermodel.js');
const rooms = require('../models/roommodel.js');
const hotels = require('../models/hotelmodel.js');
const passport = require('passport');
const strategy = require('passport-local').Strategy;
const async = require('async');

exports.login = (email,password,callback)=>{  

	user.find({email: email},(err,users)=>{  

		if(users.length != 0){  

			let temp = users[0].salt; 
			let hash_db = users[0].hashed_password; 
			// let id = users[0].token; 
			let newpass = temp + password; 
			let hashed_password = crypto.createHash('sha512').update(newpass).digest("hex"); 
			// let grav_url = gravatar.url(email, {s: '200', r: 'pg', d: '404'}); 
			// console.log("hash_db "+hash_db);
			// console.log("salt_db "+ temp);
			// console.log("hashed_password "+ hashed_password);
			if(hash_db == hashed_password){ 

				user.findOne({ email: email },(err, doc)=>{ 
					// console.log("new pass: "+ npass);
					// console.log("new salt: "+ temp1);
					// console.log("new hashed_password: "+hashed_password);  
					doc.last_login= new Date();  
					doc.save();

					async.parallel({
						rooms	:(callback)=>{
									// rooms.find({createdBy:})
									// callback(null,null);
									rooms.find({ownerId:users[0].id, isDeleted : false},'roomId hotelId roomType noOfRooms priceMRP hotelName facilities adults children imagesS3 ac',(error,rooms)=>{

										callback(error,rooms);
									});
								},
						hotels	:(callback)=>{
									hotels.find({ownerId:users[0].id, isDeleted : false},'hotelId noOfRooms hotelName imagesS3 address_line1 address_line2 address facilities policies phoneNumber mobileNumber hotelType starHotel',(error,hotels)=>{
										console.log(error);
										console.log(hotels);
										callback(error,hotels);
									});
								}
						},
						(err,results)=>{
							console.log(results);
							callback({'response':"Login Success",'res':true,
								'email':users[0].email,
								'referralCode':users[0].referralCode,
								'id':users[0].id,
								'pin':users[0].pin,
								'name':users[0].name,
								'phone':users[0].phone,
								'city':users[0].city,
								'address_line1':users[0].address_line1,
								'address_line2':users[0].address_line2,
								'address':users[0].address,
								'services':users[0].services,
								'imagesS3':users[0].imagesS3,
								'location':users[0].location,'results':results});  
						}
					);

					
				}); 

			}
			else {  

				callback({'response':"Invalid Password",'res':false});  
			} 
		}
		else {  

			callback({'response':"User doesn't exist",'res':false});  
		} 
	}); 
}