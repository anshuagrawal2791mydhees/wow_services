'use strict';
const crypto = require('crypto'); 
const rand = require('csprng'); 
const mongoose = require('mongoose'); 
const user = require('../models/usermodel.js');
const validator = require('validator');  
const id_generator = require('../utils/create_id_util');
const config = require('../config/config');


exports.register = (email,password,name,phone,city,address_line1,address_line2,address,pin,longitude,latitude,services,referralCode,gcm_token,callback) =>{  

// var referred = false;
			// console.log(referralCode);

			let temp =rand(160, 36); 
			let newpass = temp + password; 
			let hashed_password = crypto.createHash('sha512').update(newpass).digest("hex"); 

			let selected_services = JSON.parse(services);
			let current_Date = new Date();
			//console.log(city);
			
			
			let newuser = new user({ 
				email: email,
				hashed_password: hashed_password,   
				salt :temp, 
				name: name,
				phone: phone,
				city: city,
				address_line1: address_line1,
				address_line2: address_line2,
				address:address,
				pin:pin,
				location: [longitude,latitude],	
				verified:false,
				created_by:email,
				created_at:current_Date,
				modified_by:email,
				modified_at:current_Date,
				activity_status:true,
				gcm_token: gcm_token
			});
			id_generator.create_id("US",(found)=>{
				newuser.id = found['id'];
				//console.log(found['id']);
			});
			console.log(newuser);
			//let i=0;
			for(let key in selected_services)
			{
				newuser.services.push(selected_services[key]); 
			}
			  // callback(newuser);
			user.find({email: email},(err,users)=>{  

				let len = users.length;  
					 // callback('newuser');
					 // console.log('newuser');
					 console.log(err);
				if(len == 0){
					if(referralCode)
					{
						console.log("___"+referralCode);
						user.findOne({referralCode:referralCode},(err,user)=>{
							
							if(!err && user){
								user.promotionalPoints+= config.referral_reward;
								user.save((err)=>{
									if(err)
										console.log(err);
								});
								newuser.promotionalPoints+=config.referral_reward;
								newuser.save((err)=>{
									if(err)
										console.log(err);
									callback({'response':"Sucessfully Registered and added credit",'res':true,'id':newuser.id,'referralCode':newuser.referralCode});  


								});
							}
							else
							{
								newuser.save((err)=>{
									if(err)
										console.log(err);
									callback({'response':"Sucessfully Registered",'res':true,'id':newuser.id,'referralCode':newuser.referralCode});  


								});
							}

						});

					} 
					else
					{ 					console.log("fdhldldskflksdjldsfj");

						newuser.save(function(err) {   
							console.log(err);
						// console.log("password: "+ password);
						// console.log("salt: "+ temp);
						// console.log("hashed_password: "+ hashed_password);
						callback({'response':"Sucessfully Registered",'res':true,'id':newuser.id,'referralCode':newuser.referralCode});  

						});
					}
				}

				else{    
					console.log("fdhldldskflksdjldsfj");

					callback({'response':"Email already Registered",'res':false});  
				}
			});	
	

}  