'use strict';

const mongoose = require('mongoose'); 
const rooms = require('../models/roommodel');
const multer  = require('multer');
const id_generator = require('../utils/create_id_util');
const upload_s3  = require('../services/upload_s3');
const upload_grid  = require('../services/upload_grid');
const fs = require ('fs');
const path = require('path')
var Path = path.join(__dirname, '../uploads');
const shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
const deleteFolderRecursive = require('../utils/delete_directory_content');

exports.add = (req,res,callback) =>{  
	let current_Date = new Date();
	let facilitesJSON = JSON.parse(req.body.facilities);
	let newrooms = new rooms({ 
		ownerId		: req.body.ownerId,
		hotelId		: req.body.hotelId, 
		hotelName	: req.body.hotelName, 
		roomType	: req.body.roomType, 	
		noOfRooms	: req.body.noOfRooms,  
		priceMRP	: req.body.priceMRP,
		salePrice	: req.body.salePrice,
		wowPrice	: req.body.wowPrice,
		ac			: req.body.ac,
		adults		: req.body.adults,
		children	: req.body.children,  
		facilities: {
			hasWifi		: facilitesJSON['hasWifi'],
			hasDining	: facilitesJSON['hasDining'],
			hasTv 		: facilitesJSON['hasTv'],
			hasGym		: facilitesJSON['hasGym'],
			// hasBusinessService: facilitesJSON['hasBusinessService'],
			// hasCoffeeShop:      facilitesJSON['hasCoffeeShop'],
			// hasFrontDesk:       facilitesJSON['hasFrontDesk'],
			// hasSpa:             facilitesJSON['hasSpa'],
			// hasLaundry:         facilitesJSON['hasLaundry'],
			// hasOutDoorgames:    facilitesJSON['hasOutDoorgames'],
			// hasParking:         facilitesJSON['hasParking'],
			// hasRoomService:     facilitesJSON['hasRoomService'],
			// hasSwimming:        facilitesJSON['hasSwimming'],
			// hasTravelAssistance:facilitesJSON['hasTravelAssistance']             
		},
		isVerified:    false,
		createdBy:     req.body.email,
		createdAt:     current_Date,
		modifiedBy:    req.body.email,
		modifiedAt:    current_Date,
		status:        true,
		lastActivity:  current_Date
				
	});
			
	newrooms.roomId  = req.body.hotelId + "-RM-" + shortid.generate();
	//let policiesJSON = JSON.parse(req.body.policies);
	// policiesJSON.forEach((item)=>{
	// 	newhotel.policies.push(item);
	// });

	function final(count_grid,count_s3){
		if(count_s3 <=0 && count_grid<=0)
		{
			newrooms.save((err)=> { 
				callback({'res':true,"roomId":newrooms.roomId,"hotelId":newrooms.hotelId,
					"roomType":newrooms.roomType,
					"noOfRooms": newrooms.noOfRooms,
					"priceMRP":newrooms.priceMRP,
				'hotelName':newrooms.hotelName,
				'facilities': newrooms.facilities,
				'adults': newrooms.adults,
				'children': newrooms.children,
				'imagesS3': newrooms.imagesS3,
				'ac':newrooms.ac});
				deleteFolderRecursive.delete(Path,(found)=>{
					console.log(found);
				});
				
			});
		}
	};


	//console.log(req.files[0].originalname);
	let count_grid = req.files.length;
	let count_s3 = req.files.length;
	// req.files.forEach((file)=>{
 	if(req.files.length>0){
	 	for(var i=0;i<req.files.length;i++){
		 	var file_id = newrooms.roomId + "-com-" + shortid.generate();
		 	newrooms.imagesGridFS.name.push(file_id);
		 	newrooms.imagesGridFS.description.push(JSON.parse(req.body.description)[i]);
			newrooms.imagesS3.name.push(file_id);
			newrooms.imagesS3.description.push(JSON.parse(req.body.description)[i]);
		 	upload_grid.upload(req.files[i],file_id,()=>{
		 		console.log("count_grid"+count_grid);
		 		
		 		count_grid--;
		 		if(count_grid<=0){
		 			console.log("grid_done");
		 			final(count_grid,count_s3);
		 		}
		 	});	
		 
		 
		 	upload_s3.upload(req.files[i],file_id,()=>{
		 		console.log("count_s3"+count_s3)
		 		count_s3--;
		 		if(count_s3<=0){
		 			console.log('s3 done');
		 			final(count_grid,count_s3);
		 		}
		 	});
		}
	}
	else{
		newrooms.save((err)=> { 
			callback({'res':true,"roomId":newrooms.roomId,"hotelId":newrooms.hotelId,
				"roomType":newrooms.roomType,
				"noOfRooms": newrooms.noOfRooms,
				"priceMRP":newrooms.priceMRP,
				'hotelName':newrooms.hotelName,
				'facilities': newrooms.facilities,
				'adults': newrooms.adults,
				'children': newrooms.children,
				'imagesS3': newrooms.imagesS3,
				'ac':newrooms.ac
			});
		});
	}
}
exports.get_room = (req,res,callback)=>{
		
	//console.log("reached");
	//fetch only undeleted rooms
	rooms.find({roomId: req.body.roomId, isDeleted : false}, (error, rooms)=> {
		if(error){
			console.log("Error getting room: "+ req.body.roomId);
			callback({'res' : false, 'response': 'Error getting room.'});
		}
		else{
			//console.log(rooms);
			if(rooms.length == 0){
				callback({'res' : false, 'response': 'Invalid room id.'});
			}
			else{
				callback({'res' : true,
					'ownerId'	: rooms[0].ownerId,
					'roomId'	: rooms[0].roomId,
					'hotelId'	: rooms[0].hotelId,
					'hotelName'	: rooms[0].hotelName,
					'roomType'	: rooms[0].roomType,
					'noOfRooms'	: rooms[0].noOfRooms,
					'priceMRP'	: rooms[0].priceMRP,
					'salePrice'	: rooms[0].salePrice,
					'wowPrice'	: rooms[0].wowPrice,
					'ac'		: rooms[0].ac,
					'adults'	: rooms[0].adults,
					'children'	: rooms[0].children,
					'facilities'	: rooms[0].facilities,
					'imagesS3'	: rooms[0].imagesS3

				});
			}			
		}

	});
		
}

exports.update = (req,res,callback) => {

	if(!req.body.roomId)
		callback({'response':"Invalid RoomId!", 'res':false});
	else {
		let current_Date = new Date();
		let updateRoom = {};
		if(req.body.ownerId)	{ updateRoom.ownerId = req.body.ownerId}
		if(req.body.hotelId)	{ updateRoom.hotelId = req.body.hotelId }
		if(req.body.hotelName)	{ updateRoom.hotelName = req.body.hotelName }
		if(req.body.roomType)	{ updateRoom.roomType = req.body.roomType }
		if(req.body.noOfRooms)	{ updateRoom.noOfRooms = req.body.noOfRooms }
		if(req.body.priceMRP)	{ updateRoom.priceMRP = req.body.priceMRP }
		if(req.body.salePrice)	{ updateRoom.salePrice = req.body.salePrice }
		if(req.body.wowPrice)	{ updateRoom.wowPrice = req.body.wowPrice }
		if(req.body.ac)			{ updateRoom.ac = req.body.ac }
		if(req.body.adults)		{ updateRoom.adults = req.body.adults }
		if(req.body.children)	{ updateRoom.children = req.body.children }

		if(req.body.facilities)	{
			let facilitiesJSON = JSON.parse(req.body.facilities);
			let facilitiesKey = ['hasWifi', 'hasDining', 'hasTv', 'hasGym', 'hasBusinessService', 'hasCoffeeShop', 'hasFrontDesk', 'hasSpa', 'hasLaundry', 'hasOutDoorgames', 'hasParking', 'hasRoomService','hasSwimming', 'hasTravelAssistance'];
			facilitiesKey.forEach((item)=>	{
				let key = "facilities."+item;
				if(facilitiesJSON.hasOwnProperty(item)){
					updateRoom[key] = facilitiesJSON[item];
				}
			});
		}
		if(req.body.isVerified)	{ updateRoom.isVerified = req.body.isVerified } 
		
		updateRoom.modifiedBy = req.body.email;		//email always required.
		updateRoom.modifiedAt = current_Date;
		if(req.body.status)	{ updateRoom.status = req.body.status } 
		
		//Updating with updateRoom
		rooms.update({roomId: req.body.roomId},{
			$set : updateRoom
		},(err, numberAffected, rawResponse)=> {
			if(err)
				callback({'response':"Error updating room details!",'res':false});
			else{
				//console.log(numberAffected);
				if(numberAffected.n == 0)
					callback({'response':"Invalid RoomId!", 'res':false});
				else
				{
					rooms.findOne({roomId: req.body.roomId},(err,newroom)=>{
						callback({'response':"Successfully updated room details!",'res':true,rawResponse,"roomId":newroom.roomId,"noOfRooms":newroom.noOfRooms,
							'hotelName':newroom.hotelName,
							'hotelId':newroom.hotelId,
							'roomType':newroom.roomType,
							'noOfRooms':newroom.noOfRooms,
							'facilities':newroom.facilities,
							'priceMRP': newroom.priceMRP,
							'salePrice': newroom.salePrice,
							'wowPrice': newroom.wowPrice,
							'ac':newroom.ac,
							'adults':newroom.adults,
							'children':newroom.children,
							'imagesS3':newroom.imagesS3
							//change records as required
						});
						
					});
				}
			}

		});
	}//else end
}

exports.deleteRoom = (req,res,callback) => {
	if(!req.body.roomId)
		callback({'response':"Invalid RoomId!", 'res':false});
	else {
		let current_Date = new Date();
		let updateRoom = {};
		
		updateRoom.isDeleted = true;
		updateRoom.modifiedBy = req.body.email;		//email required.
		updateRoom.modifiedAt = current_Date;
		
		//Deleting by setting isDeleted true
		rooms.update({roomId: req.body.roomId},{
			$set : updateRoom
		},(err, numberAffected, rawResponse)=> {
			if(err)
				callback({'response':"Error Room not found",'res':false});
			else{
				//console.log(numberAffected);
				if(numberAffected.n == 0)
					callback({'response':"Invalid RoomId!", 'res':false});
				else
					callback({'response':"Successfully deleted room details!",'res':true});
			}
		});//update end
	} //else end
}