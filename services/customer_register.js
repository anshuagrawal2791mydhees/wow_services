'use strict';
const crypto = require('crypto'); 
const rand = require('csprng'); 
const mongoose = require('mongoose'); 
const user = require('../models/customermodel.js');
const validator = require('validator');  
const id_generator = require('../utils/create_id_util');
const config = require('../config/config');
const fs = require('fs');
const request = require('request');
const path = require('path')
let Path = path.join(__dirname, '../uploads');
const upload_s3  = require('../services/upload_s3');
const shortid = require('shortid');
const AWS = require('aws-sdk');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
const deleteFolderRecursive = require('../utils/delete_directory_content');


exports.register = (req,res,callback) =>{  

// var referred = false;
			// console.log(referralCode);

			let temp =rand(160, 36); 
			let newpass = temp + req.body.password; 
			let hashed_password = crypto.createHash('sha512').update(newpass).digest("hex"); 

			let current_Date = new Date();
			//console.log(city);
			
			
			let newuser = new user({ 
				email: req.body.email,
				hashed_password: hashed_password,   
				salt :temp, 
				name: req.body.name,
				phone: req.body.phone,
				city: req.body.city,
				
				verified:false,
				created_by:req.body.email,
				created_at:current_Date,
				modified_by:req.body.email,
				modified_at:current_Date,
				activity_status:true,
				gcm_token: req.body.gcm_token
			});
			id_generator.create_id("USC",(found)=>{
				newuser.id = found['id'];
				//console.log(found['id']);
			});

			newuser.imagesS3 = {};
			newuser.imagesS3.name = [];
			newuser.imagesS3.description =[];
			console.log(newuser);
			//let i=0;

			let download = (uri, filename, callback)=>{
				request.head(uri, (err, res, body)=>{
					// console.log('content-type:', res.headers['content-type']);
					// console.log('content-length:', res.headers['content-length']);

					let x = Path+'/'+filename;
					console.log(x)
					request(uri).pipe(fs.createWriteStream(x)).on('close', callback);
				});
			};
			
			let reg = (flag1,flag2)=>{
				if((flag1==0&&flag2==0)||(flag1==1&&flag2==1)){
					user.find({email: req.body.email},(err,users)=>{  

						let len = users.length;  
					 // callback('newuser');
					 // console.log('newuser');
					 console.log(err);
					 if(len == 0){
					 	if(req.body.referralCode)
					 	{
					 		console.log("___"+req.body.referralCode);
					 		user.findOne({referralCode:req.body.referralCode},(err,user)=>{

					 			if(!err && user){
					 				user.promotionalPoints+= config.referral_reward;
					 				user.save((err)=>{
					 					if(err)
					 						console.log(err);
					 				});
					 				newuser.promotionalPoints+=config.referral_reward;
					 				newuser.save((err)=>{
					 					if(err)
					 						console.log(err);
					 					callback({'response':"Sucessfully Registered and added credit",'res':true,'id':newuser.id,'name': newuser.name,'phone':newuser.phone,'city':newuser.city,'referralCode':newuser.referralCode,'imagesS3':newuser.imagesS3});  


					 				});
					 			}
					 			else
					 			{
					 				newuser.save((err)=>{
					 					if(err)
					 						console.log(err);
					 					callback({'response':"Sucessfully Registered",'res':true,'id':newuser.id,'name':newuser.name,'phone':newuser.phone,'city':newuser.city,'referralCode':newuser.referralCode,'imagesS3':newuser.imagesS3});  


					 				});
					 			}

					 		});

					 	} 
					 	else
					 		{ 					console.log("fdhldldskflksdjldsfj");

					 	newuser.save(function(err) {   
					 		console.log(err);
						// console.log("password: "+ password);
						// console.log("salt: "+ temp);
						// console.log("hashed_password: "+ hashed_password);
						callback({'response':"Sucessfully Registered",'res':true,'id':newuser.id,'name':newuser.name,'phone':newuser.phone,'city':newuser.city,'referralCode':newuser.referralCode,'imagesS3':newuser.imagesS3});  

					});
					 }
					}

					else{    


						callback({'response':"Email already Registered",'res':false});  
					}
				});	
				}
				
			}


			let fbProfilePicUrlFlag=0, fbProfilePicUploadFlag=0;
			if(req.body.fbProfilePicUrl){
				fbProfilePicUrlFlag=1;
			}
			else 
				reg(fbProfilePicUrlFlag,fbProfilePicUploadFlag)

			if(req.body.fbProfilePicUrl){
				download(req.body.fbProfilePicUrl, newuser.id, function(){
					console.log('downloaded');

					fs.readFile(Path+'/'+newuser.id,(err,data)=>{
						// console.log(data)
						if(err)
							console.log(err)
						else{
							let file_id = newuser.id + '-com-' + shortid.generate();
							// upload_s3.upload(Path+'/'+newuser.id,file_id,()=>{
								
							// });

							var s3 = new AWS.S3()
							s3.putObject({
 								Bucket: 'wowimagesupload', //Bucket Name
 								Key: file_id+".jpg", //Upload File Name, Default the original name
 								Body: data,
 								ContentType: data.mimetype
 							},(err, data)=>{
 								if(err)
 									console.log(err)
 								else
 								{
 									newuser.imagesS3.name.push(file_id);
									deleteFolderRecursive.delete(Path,(found)=>{
										console.log(found);
										fbProfilePicUploadFlag=1;
										reg(fbProfilePicUrlFlag,fbProfilePicUploadFlag);
									});
									
 								}
 							});
						}
					});
				});
			}

			// });

			
			


		}  