'use strict';

const mongoose = require('mongoose'); 
const fs = require ('fs');
const Grid = require('gridfs-stream');
const user = require('../models/usermodel.js');
const conn = mongoose.connection;
const multer  = require('multer');
//var storage = multer.memoryStorage();
var upload = multer({ dest: './uploads/' });
var stream = require( "stream" );
const shortid = require('shortid');
 shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');


Grid.mongo = mongoose.mongo;



module.exports.upload =(file,filename,callback)=>{


	//console.log(req.files[i].path);
	//console.log(conn);
   
    var gfs = Grid(conn.db);
 
    // streaming to gridfs
    //filename to store in mongodb
    var tempfile    = file.path;
    
	

    var origname    = filename;
    var writestream = gfs.createWriteStream({ filename: origname });

    fs.createReadStream(tempfile)
      .on('end', function() {
        // res.json('OK');
       
        console.log("ok");
        callback();

      })
      .on('error', function() {
        // res.json('ERR');
        console.log("err");
        callback();
      })
      // and pipe it to gfs
      .pipe(writestream);

      //  fs.unlink(tempfile,(err)=>{
      //   console.log(err);
      // });
      


    }

    // var writestream = gfs.createWriteStream({
    //     filename: 'mongo_file.txt'
    // });
    // fs.createReadStream('/home/etech/sourcefile.txt').pipe(writestream);
 
    // writestream.on('close', function (file) {
    //     // do something with `file`
    //     console.log(file.filename + 'Written To DB');
    // });


