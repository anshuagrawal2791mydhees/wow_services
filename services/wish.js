'use strict';

const shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

const wishModel = require('../models/wishmodel');
const wishReplyModel = require('../models/wishreplymodel');
const moment = require('moment');
const rooms = require('../models/roommodel');
const hotel = require('../models/hotelmodel');

exports.create = (req,res,callback)=>{
	
	if(!req.body.customerId || !req.body.city ) {
		console.log("customerId not specified");
		callback({'res': false, 'response':'Error creating wish. No customerId'});
	}
	else {
		let newWish = new wishModel({
			wishId		: req.body.customerId+'-1-'+shortid.generate(),
			//wishTitle	: req.body.wishTitle,
			customerId 	: req.body.customerId,
			customerName : req.body.customerName,
			city		: req.body.city,
			area		: req.body.area,
			noOfRooms 	: req.body.noOfRooms,
			checkIn 	: moment(req.body.checkIn+"+05:30"),			//make sure format correct
			checkOut 	: moment(req.body.checkOut+"+05:30"),		//make sure format correct
			adults 		: req.body.adults,
			children 	: req.body.children,
			seriousness : req.body.seriousness,
			category 	: req.body.category,
			range 		: {
				start 	: req.body.rangeStart,
				end 	: req.body.rangeEnd
			},
			negotiatedPrice 	: null,
			preferredHotelIds 	: req.body.preferredHotelIds,
			replies 	: null,
			expiresAt 	: req.body.expiresAt ? req.body.expiresAt : req.body.checkIn,
			createdBy 	: req.body.customerId,
			createdAt 	: new Date(),
			isConfirmed : false,
			isDeleted 	: false
		});
		newWish.save((err) => {
			if(err) {
				console.log("Error creating wish.");
				console.log(err);
				callback({'res': false, 'response':'Error creating wish!'});
			}
			else {
				console.log("New wish created!");
				wishModel.findById(newWish._id, (err, wish) => {
					if(err) {
						console.log("Error finding wish.");
						console.log(err);
						callback({'res': false, 'response':'Error returning wish!'});
					}
					else {
						console.log(wish);
						//send selective data
						callback(wish);
					}
				});
			}
		});
	}
}

exports.delete = (req, res, callback) => {

	//should not be deleted if confirmed or charged or refunded. or if already deleted.
	if(!req.body.wishId) {
		callback({'res' : false, 'response': 'No parameters specified.'});
	}
	else {
		let deleteWish = {
			'isDeleted' 	: true,
			'modifiedAt' 	: new Date()
		};
		wishModel.update({'wishId' : req.body.wishId},{
			$set : deleteWish
		},(err, numberAffected, rawResponse) => {
			if(err)
				callback({'response':"Error deleting wish.",'res':false});
			else {
				if(numberAffected.n == 0)
					callback({'response':"Invalid Wish Id!", 'res':false});
				else
					callback({'response':"Successfully deleted wish!",'res':true});
			}
		});
	}
}
			
exports.get = (req,res,callback)=>{
	let params = {};
	if(req.body.customerId) 	{ params.customerId = req.body.customerId }
	if(req.body.wishId) 		{ params.wishId = req.body.wishId }
	//validate check in check out too
	if(params == {})
	{
		console.log("Empty params");
		callback({'res' : false, 'response': 'No parameters specified.'});
	}
	else {
		params.isDeleted = false;
		wishModel.find(params, (error, wishes) => {
			if(error) {
				console.log("Error getting wishes: "+ params);
				callback({'res' : false, 'response': 'Error getting wishes.'});
			}
			else {
				if(wishes.length == 0) {
					callback({'res': true, 'response':'No wishes found!'});
				}
				else {
					callback({'res'	: true,
						'wishes' 	: wishes
					});
				}
			}
		});
	}
}

exports.get_wish_replies = (req,res,callback)=>{
	wishReplyModel.find({ wishId : req.body.wishId }, (error, wishReplies) => {
		if(error) {
			console.log("Error getting wish replies: "+ req.body.wishId);
			callback({'res' : false, 'response': 'Error getting wish replies.'});
		}
		else {
			callback({'res'	: true,
				'replies' 	: wishReplies
			});
		}		
	});
}

exports.get_vendor_replies = (req,res,callback)=>{
	let params = {};
	if(req.body.wishId)		{ params.wishId = req.body.wishId }
	if(req.body.ownerId) 	{ params.ownerId = req.body.ownerId }
	if(req.body.hotelId) 	{ params.hotelId = req.body.hotelId }
	if(req.body.replyId) 	{ params.hotelId = req.body.replyId }
	if(Object.getOwnPropertyNames(params).length == 0){
		callback({'res' : false, 'response': 'No paramaters specified.'});
	}
	else{
		wishReplyModel.find(params, (error, wishReplies) => {
			if(error) {
				console.log("Error getting wish replies: "+ req.body.wishId);
				callback({'res' : false, 'response': 'Error getting wish replies.'});
			}
			else {
				callback({'res'	: true,
					'replies' 	: wishReplies
				});
			}		
		});
	}
}


exports.get_wishes_vendor = (req,res,callback)=>{
	let params = {};
	if(req.body.wishId)		{ params.wishId = req.body.wishId}
	if(req.body.city) 		{ params.city = req.body.city }
	//validate check in check out too
	params.isDeleted = false;
	wishModel.find(params, (error, wishes) => {
		if(error) {
			console.log("Error getting wishes: "+ params);
			callback({'res' : false, 'response': 'Error getting wishes.'});
		}
		else {
			callback({'res'	: true,
				'wishes' 	: wishes
			});
		}
	});

}

exports.vendor_reply = (req, res, callback)=>{
	if(!req.body.price || !req.body.wishId || !req.body.roomId){
		console.log("No parameters specified. Error in wish reply.");
		callback({'res' : false, 'response': 'Parameters missing.'});
	}
	else{
		wishModel.find({ wishId : req.body.wishId, isDeleted : false }, (err, wish) => {
			if(err || wish.length == 0){
				console.log("Error/Wrong wishId in reply.");
				callback({'res' : false, 'response': 'Error getting wish.'});
			}
			else{
				wish = wish[0];
				rooms.find({ roomId : req.body.roomId, ownerId : req.body.ownerId, isDeleted : false }, 'ownerId roomId hotelName hotelId roomType', (err, room)=>{
					if(err || room.length == 0){
						console.log("Error/Wrong roomId.");
						callback({'res' : false, 'response': 'Error getting room.'});
					}
					else{

						//workaroud for hotel address
						let hoteladdress = null;
						hotel.find({hotelId : room.hotelId, isDeleted : false}, 'address', (err, hotel)=>{
							if(!err && hotel.length !=0)
								hoteladdress = hotel[0].address;
						});

						room = room[0];
						let newReply 	= new wishReplyModel({
							replyId 	: wish.wishId+'-R-'+shortid.generate(),
							wishId 		: wish.wishId,
							ownerId 	: room.ownerId,
							hotelId 	: room.hotelId,
							hotelName 	: room.hotelName,
							hotelAddress: hoteladdress,
							roomId 		: room.roomId,
							roomType 	: room.roomType,
							price 		: req.body.price,
							comment 	: req.body.comment
						});
						newReply.save((err)=> {
							if(err){
								console.log("Error saving wish reply.");
								console.log(err);
							}
							else {
								console.log("Wish reply successfull.");
								callback({'res': true, 'response' : 'Wish reply successfull.', 'reply' : newReply});
							}
						});					
					}
				});
			}
		});
	}
}

exports.confirm_reply = (req, res, callback)=>{
	if(!req.body.wishId || !req.body.replyId){
		console.log("No parameters specified. Error in confirming wish reply.");
		callback({'res' : false, 'response': 'Parameters missing.'});		
	}
	else{
		wishModel.find({ wishId : req.body.wishId }, (err, wish) => {
			if(err || wish.length == 0){
				console.log("Wrong wishId in reply.");
				callback({'res' : false, 'response': 'Invalid wishId.'});
			}
			else{
				wish = wish[0];
				if(wish.isConfirmed || wish.isDeleted){
					callback({'res' : false, 'response' : 'Wish deleted or already confirmed.'});
				}
				else{
					wishReplyModel.find({ replyId : req.body.replyId}, (err, reply)=>{
						if(err || reply.length == 0){
							console.log("Wrong reply Id.");
							callback({'res' : false, 'response': 'Invalid replyId.'});
						}
						else{
							reply = reply[0];
							wish.replyId 		= reply.replyId;
							wish.isConfirmed 	= true;
							wish.negotiatedPrice	= reply.price;
							wish.save((err)=>{
								if(err){
									console.log("Error saving reply confirmation.");
									callback({'res' : false, 'response': 'Error saving reply confirmation.'});
								}
								else{
									callback({
										'res' 		: true,
										'response' 	: 'Wish negotiation confirmed.',
										'wish' 		: wish
									});
								}
							});
						}
					});
				}

			}
		});
	}
}