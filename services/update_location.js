'use strict';
const mongoose = require('mongoose'); 
const user = require('../models/usermodel.js');

exports.update_location = (email,address,longitude,latitude,callback)=>{
	user.update({email: email},{
		address: address,
		location: [longitude,latitude]
	},(err, numberAffected, rawResponse)=> {
		if(err)
			callback({'response':"Error while storing location!",'res':false});
		else
			callback({'response':"Success!",'res':true});

	});
}