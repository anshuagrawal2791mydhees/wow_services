'use strict';
const hotel = require('../models/hotelmodel');

exports.submit = (req,res,callback)=>{

	hotel.findOne({'hotelId':req.body.hotelId},(err,hotel)=>{

		console.log(req.body);

		if(err)
			console.log(err);
		else{
			hotel.numberOfReviews++;
			let n = hotel.numberOfReviews;
			//console.log(n);
			//console.log(hotel.userRatings.frontOfficeStaff);
			//console.log("n--"+n);
			var y = (hotel.userRatings.frontOfficeStaff*(n-1))/n;
			y+=(req.body.frontOfficeStaff)/n;
			hotel.userRatings.frontOfficeStaff = y;
			//console.log(hotel.userRatings.frontOfficeStaff);
			var y = (hotel.userRatings.restaurantAmbiance*(n-1))/n;
			y+=(req.body.restaurantAmbiance)/n;
			hotel.userRatings.restaurantAmbiance =y;
			
			var y = (hotel.userRatings.restaurantStaff*(n-1))/n;
			y+=(req.body.restaurantStaff)/n;
			hotel.userRatings.restaurantStaff = y;
			
			var y = (hotel.userRatings.houseKeeping*(n-1))/n;
			y+=(req.body.houseKeeping)/n;
			hotel.userRatings.houseKeeping = y;
			
			var y = (hotel.userRatings.cleanliness*(n-1))/n;
			y+=(req.body.cleanliness)/n;	
			hotel.userRatings.cleanliness = y;
			
			var y = (hotel.userRatings.checkInProcedure*(n-1))/n;
			y+=(req.body.checkInProcedure)/n;
			hotel.userRatings.checkInProcedure = y;
			
			var y = (hotel.userRatings.likelyToStayAgain*(n-1))/n;
			y+=(req.body.likelyToStayAgain)/n;
			hotel.userRatings.likelyToStayAgain = y;
			
			var y = (hotel.userRatings.overall*(n-1))/n;
			y+=(req.body.overall)/n;
			hotel.userRatings.overall = y;

			let a = {
				'review': req.body.review,
				'overallRating': req.body.overall,
				'customerId': req.body.customerId,
				'customerName': req.body.customerName,
				'roomId': req.body.roomId,
				'roomType' : req.body.roomType
			}
			hotel.reviews.push(a);
			console.log(hotel);
			hotel.save((err,response)=>{
				if(err)
					console.log(err)
				else
					callback({'response':response,'res':true});
			});
			

		}
	});

}