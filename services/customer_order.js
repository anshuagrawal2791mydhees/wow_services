'use strict';

const shortid = require('shortid');
 shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

const expresscheckout = require('expresscheckout');
const configs = require('../config/config.json');
config.api_key = configs.juspay_configs.api_key;
// console.log(configs.juspay_configs.api_key);
config.environment = 'staging';
const orderModel = require('../models/ordermodel');
const customer = require('../models/customermodel');
const partner = require('../models/usermodel');
const offers = require('../models/offermodel');
const wishModel = require('../models/wishmodel');
const wishReplyModel = require('../models/wishreplymodel');
const schedule_notif = require('../utils/schedule_notif.js');
const customer_schedule_notif = require('../utils/customer_schedule_notif.js');
const moment = require('moment');
const send_notif_partner = require('../utils/send_notif.js');

exports.create = (req,res,callback)=>{
	console.log(req.body.customerId);
	//check offer validity first
	//check customerId validation
	let offer = null;
	offers.find({'offerId': req.body.offerId, 'isDeleted': false}, 'offerId offerName hotelId hotelName ownerId offerName wowPrice roomId roomType checkIn checkOut',(err, offer)=>{
		if(err || offer.length == 0) {
			console.log("Error getting offer!");
			callback({'res':false, 'response': 'Error getting offer details!'});
		}
		else{
			offer = offer[0];
			let disc_amt = 0; //from discountId
			//let gross_amt = offer.wowPrice*req.body.noOfRooms;
			//REMOVE ASAP
			if(!req.body.noOfRooms)
				req.body.noOfRooms = 1;

			let gross_amt = offer.wowPrice * parseInt(req.body.noOfRooms);
			let total_amt = gross_amt - disc_amt;

			//AMOUNT CALCULATION

			console.log(offer);
			//console.log("888888888");
			let neworder = new orderModel({
				order_id	: req.body.customerId+'-O-'+shortid.generate(),
				customer_id	: req.body.customerId,
				orderType 	: "offer",
				ownerId		: offer.ownerId,
				hotelId 	: offer.hotelId,
				roomId 		: offer.roomId,	//or should be taken from offermodel
				booking		: {
					name		: req.body.bookingName,
					contact		: req.body.bookingContact,
					checkIn		: offer.checkIn,
					checkOut	: offer.checkOut,
					confirmed	: false,
					hotelName	: offer.hotelName,
					roomType	: offer.roomType,
					noOfRooms 	: req.body.noOfRooms,
					offerId 	: req.body.offerId,
					offerName 	: offer.offerName
				},
				offerId		: req.body.offerId,
				offerName	: offer.offerName,
				status 		: "PENDING",					//101 status for blocked
				statusId	: 101,
				discountId	: req.body.discountId,
				discountAmount : req.body.discountAmount, 	//should be calculated here
				amount 		: total_amt,		//should be verified and saved
				currency	: req.body.currency,	//default should be set
				createdBy	: req.body.customerId	//should be modified to req.user.id
			});
			console.log(neworder);
			neworder.save((err) => {
				if(err) {
					console.log("Error creating order.");
					console.log(err);
					callback({'res': false, 'response':'Error creating order!'});
				}
				else {
					Orders.create({
						order_id		: neworder.order_id,
						amount			: neworder.amount,
						customer_id		: neworder.customer_id,
						customer_phone	:req.body.phone,	//req.user.phone
						customer_email	:req.body.email,	//req.user.email
						description		:req.body.description,
						billing_address_first_name		:neworder.booking.bookingName,
						//billing_address_city			:req.body.city,
						//billing_address_postal_code		:req.body.pin,
						billing_address_phone			:neworder.booking.bookingContact
					}, (response,err) =>{
						if(err) {
							console.log(err);
							callback({'res': false, 'response':'Error creating order!'})
							//delete order ****
						}
						else {
							//blocking room
							offers.update({ offerId : req.body.offerId },
							{ $inc : { bookedRooms : 1 } },
							(err, numberAffected, rawResponse) => {
								if(err || numberAffected == 0)
									console.log("Error blocking offer." + req.body.offerId);
							});
							
							//if(response.status_id == 1)
							console.log(response);
							callback(neworder);
							//callback(response);
						}
					});
				}
			});
		}
	});
}

exports.create_wish_order = (req,res,callback)=>{
	console.log(req.body.customerId);
	//check wish validity first
	//check customerId validation
	let wish = null;

	//wishValidate(req, res, (error, currentWish)=>);

	wishModel.find({'wishId': req.body.wishId, 'isConfirmed': true, 'isDeleted': false}, 'wishId replyId negotiatedPrice checkIn checkOut',(err, wish)=>{
		if(err || wish.length == 0) {
			console.log("Error getting wish!");
			callback({'res':false, 'response': 'Error getting wish details!'});
		}
		else{
			wish = wish[0];
			wishReplyModel.find({ replyId : wish.replyId }, 'ownerId hotelId hotelName roomId price', (err, reply)=>{
				if(err || reply.length == 0) {
					console.log("Error getting wishreply!");
					callback({'res':false, 'response': 'Error getting wish details!'});
				}
				else {
					reply = reply[0];
					let disc_amt = 0; //from discountId
					//let gross_amt = offer.wowPrice*req.body.noOfRooms;
					let gross_amt = wish.negotiatedPrice * wish.noOfRooms;
					let total_amt = gross_amt - disc_amt;
					console.log(wish);
					console.log(reply);
					let neworder = new orderModel({
						order_id	: req.body.customerId+'-O-'+shortid.generate(),
						customer_id	: req.body.customerId,
						orderType 	: "wish",
						ownerId		: reply.ownerId,
						hotelId 	: reply.hotelId,
						roomId 		: reply.roomId,	//or should be taken from offermodel
						booking		: {
							name	: req.body.bookingName,
							contact	: req.body.bookingContact,
							checkIn	: wish.checkIn,
							checkOut	: wish.checkOut,
							confirmed	: false,
							hotelName	: reply.hotelName,
							roomType	: reply.roomType,
							noOfRooms 	: wish.noOfRooms,
							wishId 		: wish.wishId,
							replyId 	: reply.replyId
						},
						status 		: "PENDING",					//101 status for blocked
						statusId	: 101,
						discountId	: req.body.discountId,
						discountAmount : req.body.discountAmount, 	//should be calculated here
						amount 		: total_amt,		//should be verified and saved
						currency	: req.body.currency,	//default should be set
						createdBy	: req.body.customerId	//should be modified to req.user.id
					});
					console.log(neworder);
					neworder.save((err) => {
						if(err) {
							console.log("Error creating order.");
							console.log(err);
							callback({'res': false, 'response':'Error creating order!'});
						}
						else {
							Orders.create({
								order_id		: neworder.order_id,
								amount			: neworder.amount,
								customer_id		: neworder.customer_id,
								customer_phone	:req.body.phone,	//req.user.phone
								customer_email	:req.body.email,	//req.user.email
								description		:req.body.description,
								billing_address_first_name		:neworder.booking.bookingName,
								//billing_address_city			:req.body.city,
								//billing_address_postal_code		:req.body.pin,
								billing_address_phone			:neworder.booking.bookingContact
							}, (response,err) =>{
								if(err) {
									console.log(err);
									callback({'res': false, 'response':'Error creating order!'})
									//delete order ****
								}
								else {
									callback(neworder);
								}
							});
						}
					});
				}
			});			
		}
	});
}


exports.status = (req,res,callback)=>{

	Orders.get_status({order_id: req.body.orderId},(response,err)=>{
		if(err){
			console.log("Error getting order status: "+req.body.orderId);
			callback({'res':false, 'response':'Error fetching order status'});
		}
		else {
			//update ordermodel status
			//console.log(response);
			orderModel.update({order_id: req.body.orderId},{
				$set : {
					status : response.status,
					statusId : response.status_id
				}
			},(err, numberAffected, rawResponse) => {
				if(err)
					callback({'response':"Error updating order status in record!",'res':false});
				else
				{
					orderModel.findOne({'order_id': req.body.orderId}, (err,getorder)=>{
						if(err)
							callback({'response':"Error getting order status from records!",'res':false});	
						else {
							getorder = getorder.toJSON();
							getorder.transaction = response;
							callback(getorder);
						}
					});
				}
			});
		}
	});

}

exports.status_callback = (req,res,callback)=>{

	Orders.get_status({order_id: req.body.orderId},(response,err)=>{
		if(err){
			console.log("Error getting order status: "+req.body.orderId);
			callback({'res':false, 'response':'Error fetching order status'});
		}
		else {
			//update ordermodel status
			console.log(response);
			orderModel.find({ order_id : req.body.orderId, status : "PENDING" }, 'status statusId orderType booking', (err, order)=>{
				if(err || order.length == 0) {
					console.log("Error getting order "+req.body.orderId);
					callback({'res':false, 'response':'Error fetching order/ already paid.'});
				}
				else {
					//Unblocking room if not charged
					if(order.orderType == "offer" && response.status != "CHARGED"){
						offers.update({ offerId : order.booking.offerId },
							{ $inc : { bookedRooms : (booking.noOfRooms* -1) } },
							(err, numberAffected, rawResponse) => {
								if(err || numberAffected == 0)
									console.log("Error unblocking offer." + req.body.offerId);
							}
						);
					}
					orderModel.update({order_id: req.body.orderId},{
						$set : {
							status : response.status,
							statusId : response.status_id
						}
					},(err, numberAffected, rawResponse) => {
						if(err)
							callback({'response':"Error updating order status in record!",'res':false});
						else
						{
							orderModel.findOne({'order_id': req.body.orderId}, (err,getorder)=>{
								if(err)
									callback({'response':"Error getting order status from records!",'res':false});	
								else {
									//setting alarms for checkin and checkout notifs
									//console.log("------"+getorder);
									if(getorder.get('status')=='CHARGED')
									{ //placed temporary gcm user id
											customer.findOne({'id':getorder.customer_id},'gcm_token',(err,response)=>{
												let metadata1 = {"details":getorder,"click_action":"checkIn"};
												//console.log(response.gcm_token);
												customer_schedule_notif.schedule(getorder.booking.checkIn,'checkIn Alarm','Time for CheckIn',
												response.gcm_token,metadata1,(found)=>{
													
														console.log(found);
												});
												let metadata2 = {"details":getorder,"click_action":"Feedback"};
												console.log("-----"+JSON.stringify(metadata2));
											customer_schedule_notif.schedule(getorder.booking.checkOut,'Feedback','Tell us about your experience',
												response.gcm_token,metadata2,(found)=>{
														console.log(found);
												});
											});	
											partner.findOne({'id':getorder.ownerId},'gcm_token',(err,response)=>{
												if(err)
													console.log(err);
												else{
													let metadata3 = {"details":getorder,"click_action":"Converted"};
													send_notif_partner.send('Booking','New Booking',response.gcm_token,metadata3,(found)=>{
														console.log(found);
													});
												}
											});										
									}
									getorder = getorder.toJSON();
									getorder.transaction = response;
									callback(getorder);
								}
							});
						}
					});
				}
			});
			
		}
		//callback(response);
	});

}

//status2


exports.list = (req, res, callback)=>{
	orderModel.find({customer_id : req.body.customerId}, (error, orders)=>{
		if(error){
			console.log("Error getting orders: "+ req.body.customerId);
			callback({'res' : false, 'response': 'Error getting orders.'});
		}
		else {
			if(orders.length == 0){
				callback({'res' : false, 'response': 'Invalid customer id or no order presesnt.'});
			}
			else {
				
				callback({
					'res'		: true,
					'orders'	: orders
					});
			}
		}
	});
}

exports.booked_offers = (req, res, callback)=>{
	let params = {};
	if(req.body.ownerId)	params.ownerId = req.body.ownerId;
	if(req.body.offerId)	params.offerId = req.body.offerId;
	//edit mongoose find query to accomodate multiple values of status
	if(req.body.status)		params.status = req.body.status;
	if(req.body.statusId)		params.statusId = req.body.statusId;
	if(req.body.confirmed)		params.booking.confirmed = req.body.confirmed;
	if(params)
	{	
		orderModel.find(params, (error, orders)=>{
			if(error){
				console.log("Error getting orders: "+ params);
				callback({'res' : false, 'response': 'Error getting orders.'});
			}
			else {
				if(orders.length == 0){
					callback({'res' : false, 'response': 'No order found.'});
				}
				else {
					callback({
						'res'		: true,
						'orders'	: orders
						});
				}
			}
		});
	}
	else{
		callback({'res' : false, 'response': 'No valid parameter specified.'});
	}
}
exports.booked_offers2 = (req, res, callback)=>{
	
		orderModel.update({}, (error, orders)=>{
			if(error){
				console.log("Error getting orders: "+ params);
				callback({'res' : false, 'response': 'Error getting orders.'});
			}
			else {
				if(orders.length == 0){
					callback({'res' : false, 'response': 'No order found.'});
				}
				else {
					callback({
						'res'		: true,
						'orders'	: orders
						});
				}
			}
		});
}

//exports.get_rooms_available = ()

// console.log(config.api_key);
/*
const wishValidate = (req, res, currentWish) =>{
	wishModel.find({'wishId': req.body.wishId, 'isConfirmed': true, 'isDeleted': false}, 'wishId replyId negotiatedPrice checkIn checkOut',(err, wish)=>{
		if(err || wish.length == 0) {
			console.log("Error getting wish!");
			currentWish({'res':false, 'response': 'Error getting wish details!'});
		}
		else{
			wish = wish[0];
			wishReplyModel.find({ replyId : wish.replyId }, 'ownerId hotelId hotelName roomId price', (err, reply)=>{
				if(err || reply.length == 0) {
					console.log("Error getting wishreply!");
					currentWish({'res':false, 'response': 'Error getting wish details!'});
				}
				else {

				}
			});
		}
	});
}
*/