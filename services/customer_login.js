'use strict';
const crypto = require('crypto'); 
const rand = require('csprng'); 
const mongoose = require('mongoose'); 
// var gravatar = require('gravatar'); 
const user = require('../models/customermodel.js');

const passport = require('passport');
const strategy = require('passport-local').Strategy;
const async = require('async');

exports.login = (email,password,fb,callback)=>{  

	user.find({email: email},(err,users)=>{  

		if(users.length != 0){  

			let temp = users[0].salt; 
			let hash_db = users[0].hashed_password; 
			// let id = users[0].token; 
			let newpass = temp + password; 
			let hashed_password = crypto.createHash('sha512').update(newpass).digest("hex"); 
			// let grav_url = gravatar.url(email, {s: '200', r: 'pg', d: '404'}); 
			// console.log("hash_db "+hash_db);
			// console.log("salt_db "+ temp);
			// console.log("hashed_password "+ hashed_password);
			if(hash_db == hashed_password || fb==1){ 

				user.findOne({ email: email },(err, doc)=>{ 
					// console.log("new pass: "+ npass);
					// console.log("new salt: "+ temp1);
					// console.log("new hashed_password: "+hashed_password);  
					doc.last_login= new Date();  
					doc.save();

					
						
							//console.log(results);
							callback({'response':"Login Success",'res':true,
								'email':users[0].email,
								'referralCode':users[0].referralCode,
								'id':users[0].id,
								'pin':users[0].pin,
								'name':users[0].name,
								'phone':users[0].phone,
								'city':users[0].city,
								'imagesS3' : users[0].imagesS3
								});  
						}
					);

					
				

			}
			else {  

				callback({'response':"Invalid Password",'res':false});  
			} 
		}
		else {  

			callback({'response':"User doesn't exist",'res':false});  
		} 
	}); 
}