'use strict';

const shortid = require('shortid');
 shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

const bookingHotelModel = require('../models/bookinghotelmodel');
const offerModel = require('../models/offermodel');
const wishModel = require('../models/wishmodel');
const wishReplyModel = require('../models/wishreplymodel');
const orderModel = require('../models/ordermodel');
const customer = require('../models/customermodel');
const schedule_notif = require('../utils/schedule_notif.js');
const customer_schedule_notif = require('../utils/customer_schedule_notif.js');
const send_notif_partner = require('../utils/send_notif.js');

exports.create = (req,res,callback)=>{
	console.log(req.body.customerId);
	let bookingDetails = {};
	bookingDetails.checkIn 		= req.body.checkIn;
	bookingDetails.checkOut 	= req.body.checkOut;
	bookingDetails.ownerId 		= req.body.ownerId;
	bookingDetails.hotelId 		= req.body.hotelId;
	bookingDetails.roomId 		= req.body.roomId;
	bookingDetails.roomType 	= req.body.roomType;
	bookingDetails.noOfRooms 	= req.body.noOfRooms;
	bookingDetails.adults 		= req.body.adults

	let newBooking = new bookingHotelModel({
		bookingId 		: req.body.customerId+'-B'+shortid.generate(),
		customerId 		: req.body.customerId,
		bookingName 	: req.body.bookingName,
		bookingContact	: req.body.bookingContact,
		//checkIn 		: 
	});

	let neworder = new orderModel({
		order_id	: req.body.customerId+'-O-'+shortid.generate(),
		customer_id	: req.body.customerId,
		orderType 	: "offer",
		ownerId		: offer.ownerId,
		hotelId 	: offer.hotelId,
		roomId 		: offer.roomId,	//or should be taken from offermodel
		booking		: {
			name		: req.body.bookingName,
			contact		: req.body.bookingContact,
			checkIn		: offer.checkIn,
			checkOut	: offer.checkOut,
			confirmed	: false,
			hotelName	: offer.hotelName,
			roomType	: offer.roomType,
			noOfRooms 	: req.body.noOfRooms,
			offerId 	: req.body.offerId,
			offerName 	: offer.offerName
		},
		offerId		: req.body.offerId,
		offerName	: offer.offerName,
		status 		: "PENDING",					//101 status for blocked
		statusId	: 101,
		discountId	: req.body.discountId,
		discountAmount : req.body.discountAmount, 	//should be calculated here
		amount 		: total_amt,		//should be verified and saved
		currency	: req.body.currency,	//default should be set
		createdBy	: req.body.customerId	//should be modified to req.user.id
	});
	console.log(neworder);
	neworder.save((err) => {
		if(err) {
			console.log("Error creating order.");
			console.log(err);
			callback({'res': false, 'response':'Error creating order!'});
		}
		else {
			Orders.create({
				order_id		: neworder.order_id,
				amount			: neworder.amount,
				customer_id		: neworder.customer_id,
				customer_phone	:req.body.phone,	//req.user.phone
				customer_email	:req.body.email,	//req.user.email
				description		:req.body.description,
				billing_address_first_name		:neworder.booking.bookingName,
				//billing_address_city			:req.body.city,
				//billing_address_postal_code		:req.body.pin,
				billing_address_phone			:neworder.booking.bookingContact
			}, (response,err) =>{
				if(err) {
					console.log(err);
					callback({'res': false, 'response':'Error creating order!'})
					//delete order ****
				}
				else {
					//blocking room
					offers.update({ offerId : req.body.offerId },
					{ $inc : { bookedRooms : 1 } },
					(err, numberAffected, rawResponse) => {
						if(err || numberAffected == 0)
							console.log("Error blocking offer." + req.body.offerId);
					});
					
					//if(response.status_id == 1)
					console.log(response);
					callback(neworder);
					//callback(response);
				}
			});
		}
	});
}

//exports.get_rooms_available = ()


exports.createBooking = (req,res,callback)=>{
	console.log(req.body.customerId);
	//implement discount model
	var newBooking = new bookingHotelModel();
	newBooking.bookingId = req.body.customerId+'-B'+shortid.generate();
	newBooking.bookingType 	= req.body.bookingType ? req.body.bookingType : "None";
	if(newBooking.bookingType == "offer")
	{
		getBookingFromOffer(newBooking, (err, newBooking)=>{
			if(err) {
				console.log(err);
				return callback({ 'res': false, 'response': err});
			}
			else {
				newBooking.save((err)=> {
					if(err){
						console.log("Error creating booking.");
						console.log(err);
						return callback({ 'res': false, 'response': "Error creating booking."});
					}
					else{
						console.log("New booking : "+ newBooking.bookingId);
						return callback({ 'res': true, 'response' : "New Booking Successful",
							'booking' : newBooking
						});
					}
				});
			}
		});
	}
	else if(newBooking.bookingType == "wish")
	{
		getBookingFromWish(newBooking, (err, newBooking)=>{
			if(err) {
				console.log(err);
				return callback({ 'res': false, 'response': err});
			}
			else {
				newBooking.save((err)=> {
					if(err){
						console.log("Error creating booking.");
						console.log(err);
						return callback({ 'res': false, 'response': "Error creating booking."});
					}
					else{
						console.log("New booking : "+ newBooking.bookingId);
						return callback({ 'res': true, 'response' : "New Booking Successful",
							'booking' : newBooking
						});
					}
				});
			}
		});
	}
	else
	{
		console.log("none type booking");
		newBooking.save((err)=> {
			if(err){
				console.log("Error creating booking.");
				console.log(err);
				return callback({ 'res': false, 'response': "Error creating booking."});
			}
			else{
				console.log("New booking : "+ newBooking.bookingId);
				return callback({ 'res': true, 'response' : "New Booking Successful",
					'booking' : newBooking
				});
			}
		});
	}
}

//function to populate booking details
function populateBookingDetails(err, newBooking, callback){
	if(req.body.bookingDetails);
	let bookingDetails = toJSON(req.body.bookingDetails);
	newBooking.bookingDetails = bookingDetails;
	//if details not present then get from user.
	if(req.body.bookingName) newBooking.bookingName = req.body.bookingName;
	if(req.body.bookingContact) newBooking.bookingContact = req.body.bookingContact;
	if(req.body.bookingEmail) newBooking.bookingEmail = req.body.bookingEmail;
	if(req.body.currency) newBooking.currency = req.body.currency;
	return bookingDiscount(err, newBooking, callback);
}

function bookingDiscount(err, newBooking, callback){
	if(req.body.discountId) newBooking.discountId = req.body.discountId;
	//lololol add discount model
	if(req.body.discountAmount){
		newBooking.discountAmount = req.body.discountAmount;
		newBooking.amount = newBooking.amount - newBooking.discountAmount;
	}
	return callback(err, newBooking);
}

//function to fetch offer
function getBookingFromOffer(newBooking, callback){

}
//function to fetch wish
function getBookingFromWish(newBooking, callback){
	wishModel.find({"wishId" : req.body.wishId, isDeleted : false}, (err, wish)=>{
		if(err || wish.length == 0){
			console.log("Invalid wishId");
			console.log(req.body.wishId);
			return populateBookingDetails(new Error("Invalid Wish Id"), newBooking, callback);
		}
		else{
			wish = wish[0];
			//wish.isConfirmed
			if(!wish.isConfirmed || !wish.replyId){
				console.log("Wish not confirmed yet");
				console.log(wish.wishId);
				console.log(wish.replyId);
				return populateBookingDetails(new Error("Wish not confirmed yet"), newBooking, callback);
			}
			else{
				newBooking.customerId = wish.customerId;
				newBooking.bookingName = wish.customerName;
				newBooking.checkIn = wish.checkIn;
				newBooking.checkOut = wish.checkOut;
				newBooking.noOfRooms = wish.noOfRooms;
				newBooking.adults = wish.adults;
				newBooking.children = wish.children;
				newBooking.orderId = null;
				newBooking.rate = wish.negotiatedPrice;
				newBooking.amount = newBooking.rate * newBooking.noOfRooms;
				newBooking.status = 1;
			//	newBooking.modifiedBy = req.user.id;
				wishReplyModel.find({"replyId" : wish.replyId}, (err, reply)=> {
					if(err || reply.length == 0){
						console.log("Invalid wishReplyId");
						console.log(wish.wishId);
						return populateBookingDetails(new Error("Invalid Wish Reply Id"), newBooking, callback);
					}
					else{
						reply = reply[0];
						newBooking.ownerId		= newBooking.ownerId;
						newBooking.hotelId		= newBooking.hotelId;
						newBooking.hotelName	= newBooking.hotelName;
						newBooking.roomId		= newBooking.roomId;
						newBooking.roomType		= newBooking.roomType;
						return populateBookingDetails(null, newBooking, callback);
					}
				});
			}
		}
	});
}
//function to update booking details
//exports.get_rooms_available = ()
