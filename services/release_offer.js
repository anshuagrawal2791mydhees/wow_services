'use strict';
const offer = require('../models/offermodel.js');
const shortid = require('shortid');
const id_generator = require('../utils/create_id_util');
const moment = require('moment');
const user = require('../models/usermodel');
const config = require('../config/config');

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

exports.release = (req,res,callback)=>{

// console.log(req.body);
let current_date = new Date();

	user.findOne({id:req.body.ownerId},'totalPoints purchasedPoints promotionalPoints',(err,user)=>{
		// console.log(user.purchasedPoints);
		if(user.totalPoints<(config.release_offer_cost))
		{
			callback({'res':false,'response':'not enough points'});
		}
		else{

			let newoffer = new offer({
		ownerId: req.body.ownerId,
		hotelId: req.body.hotelId,
		hotelName: req.body.hotelName,
		// offerId: req.body.offerId,
		offerName: req.body.offerName,
		roomId: req.body.roomId,
		roomType: req.body.roomType,
		noOfRooms: req.body.noOfRooms,
		priceMRP: req.body.priceMRP,
		wowPrice: req.body.wowPrice,
		checkIn: moment(req.body.checkIn+"+05:30"),
		checkOut: moment(req.body.checkOut+"+05:30"),
		scheduledAt: moment(req.body.scheduledAt+"+05:30"),
		expiresAt:  moment(req.body.expiresAt+"+05:30"),
		createdBy: req.body.email,
		modifiedBy: req.body.email,
		createdAt: current_date,
		modifiedAt: current_date,
	});
	newoffer.offerId = req.body.roomId+"-OF-"+shortid.generate();

	newoffer.save((err)=>{
		// user.purchasedPoints -= parseInt(config.release_offer_cost);
		user.totalPoints -= (config.release_offer_cost);
		if(user.purchasedPoints>=(config.release_offer_cost))
			user.purchasedPoints-= (config.release_offer_cost);
		else if (user.purchasedPoints< user.promotionalPoints)
		{
			user.promotionalPoints -= ((config.release_offer_cost)-user.purchasedPoints);
			user.purchasedPoints=0;
		}
		else if(user.promotionalPoints>= (config.release_offer_cost))
			user.promotionalPoints-= (config.release_offer_cost);
		
		else {
			user.purchasedPoints -= ((config.release_offer_cost)-user.promotionalPoints);
			user.promotionalPoints=0;
		}
		// console.log("++++++++++"+user.totalPoints);
		user.save((err)=>{
		if(err)
		console.log(err);
		callback({'res':true,
			'offerId':newoffer.offerId,
			'offerName':newoffer.offerName,
			'hotelId':newoffer.hotelId,
			'hotelName': newoffer.hotelName,
			'roomId': newoffer.roomId,
			'roomType': newoffer.roomType,
			'noOfRooms': newoffer.noOfRooms,
			'priceMRP': newoffer.priceMRP,
			'wowPrice': newoffer.wowPrice,
			'checkIn': newoffer.checkIn,
			'checkOut':newoffer.checkOut,
			'scheduledAt': newoffer.scheduledAt,
			'expiresAt': newoffer.expiresAt,
			'createdAt': newoffer.createdAt
		});
		});
		
	});


	// console.log("here");
	// callback("f");


		}
	});
	
}
