'use strict';
const partner = require('../models/usermodel');
const multer  = require('multer');
const id_generator = require('../utils/create_id_util');
// var uploads = multer({ dest: 'uploads/' });
const upload_s3  = require('../services/upload_s3');
const fs = require ('fs');
const path = require('path')
var Path = path.join(__dirname, '../uploads');
const shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
const deleteFolderRecursive = require('../utils/delete_directory_content');

exports.upload = (req,res,callback)=>{
	partner.findOne({'id': req.body.id},(err,user)=>{
		var file_id = req.body.id + "-com-" + shortid.generate();
		user.imagesS3.name.push(file_id);
		user.imagesS3.description.push(JSON.parse(req.body.description)[0]);
		upload_s3.upload(req.files[0],file_id,()=>{
			
			console.log('s3 done');
				//final(count_grid,count_s3);
				user.save((err,resp)=>{
					if(err)
						console.log(err);
					else{
						callback({'res':true,'response':"Successful",'id':file_id});
						deleteFolderRecursive.delete(Path,(found)=>{
							console.log(found);
						});
					}
				});
				


			});
	});
}