'use strict';
const mongoose = require('mongoose'); 
const user = require('../models/usermodel.js');
const validator = require('validator');  

exports.check_user = (email,callback)=>{
	let x = email;
	if(validator.isEmail(x)){ 
		user.find({email: email},function(err,users){  
			let len = users.length;
			if(len == 0){   
				callback({'response':"Success",'res':true});
			}
			else{    
				callback({'response':"Email already Registered",'res':false});
			}
		});	
	}
	else{
		callback({'response':"Email Not Valid",'res':false}); 
	}

} 
