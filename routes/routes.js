'use strict';
const qs = require('querystring');
const register = require('../services/register'); 
const login = require('../services/login');
const resetPass = require('../services/resetpass');
const update_location = require('../services/update_location');
const check_user = require('../services/check_user');
const check_user_customer = require('../services/check_user_customer');
const upload_grid  = require('../services/upload_grid');
const upload_s3  = require('../services/upload_s3');
//new route
const hotel = require('../services/hotel');
const room = require('../services/room');
const customer_order = require('../services/customer_order');
const release_offer = require('../services/release_offer');
const get_offers = require('../services/get_offers');
const get_credits = require('../services/get_credits');
const get_offer_by_owner = require('../services/get_offer_by_owner');
const wish = require('../services/wish');
const order = require('../services/order');
const hotelBooking = require('../services/booking_hotel');
const points = require('../services/points');

const contactUs = require('../services/contact_us');

const multer  = require('multer');
var storage = multer.memoryStorage();
var uploads = multer({ dest: 'uploads/' });
const id = require('../utils/create_id_util');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const send_notif = require('../utils/send_notif.js');
const customer_send_notif = require('../utils/customer_send_notif.js');
const schedule_notif = require('../utils/schedule_notif.js');
const register_customer = require('../services/customer_register.js');
const customer_login = require('../services/customer_login.js');
const feedback = require('../services/feedback.js');
const upload_partner_image = require('../services/upload_partner_image.js');
const upload_customer_image = require('../services/upload_customer_image.js');
const redeem_history = require('../services/redeem_history.js');
const get_city_list = require('../services/get_city_list');


// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}

module.exports = (app)=> {        
    //app.use(passport.initialize());
    //app.use(passport.session());


    app.get('/', function(req, res) {       

      res.end("Node-Android-Projecti");    
    });

     app.post('/register',function(req,res){
           console.log(req.body);
           let email = req.body.email;             
           let password = req.body.password; 
           let name = req.body.name;
           let phone = req.body.phone;
           let city = req.body.city;
           let address_line1 = req.body.address_line1;
            let address_line2 = req.body.address_line2;
              let address = req.body.address;
               let longitude = req.body.longitude;
               let latitude = req.body.latitude;
               let services = req.body.services;
               let pin = req.body.pin;
               let gcm_token = req.body.gcm_token;
                console.log(email);

           register.register(email,password,name,phone,city,address_line1,address_line2,address,pin,longitude,latitude,services,req.body.referralCode,gcm_token,(found)=>{             
                console.log(found);             
               res.json(found);    
        });     
      });

     app.post('/register/customer',(req,res)=>{
        console.log(req.body);
        register_customer.register(req,res,(found)=>{
          res.json(found);
        })

     });
     app.post('/login/customer',(req,res)=>{
        customer_login.login(req.body.email,req.body.password,req.body.fb,(found)=>{
            res.json(found);
        });
     });

       app.post('/login',(req,res)=>{        
          

          let email = req.body.email;             
          let password = req.body.password;       

          login.login(email,password,function (found) {           
              // console.log(found);             
              res.json(found);    
        });    
      });     

       app.post('/api/resetpass',(req, res)=> {         

          let email = req.body.email;         

          resetPass.respass_init(email,(found)=>{             
               // console.log(found);             
               res.json(found);    
        });     
      }); 

       app.post('/api/resetpass/chg',(req, res)=> {         
          let email = req.body.email;         
          let code = req.body.code;       
          let npass = req.body.npass;       

        resetPass.respass_chg(email,code,npass,(found)=>{           
          // console.log(found);             
          res.json(found);    
     
        });     
       }); 

       app.post('/update_location',(req,res)=>{
        let email = req.body.email;
        let address = req.body.address;
        let latitude = req.body.latitude;
        let longitude = req.body.longitude;
        update_location.update_location(email,address,longitude,latitude,(found)=>{
          res.json(found);
        });

       }); 
       app.post('/check_user',(req,res)=>{
        let email = req.body.email;
        check_user.check_user(email,(found)=>{
          res.json(found);
        });
       });
       app.post('/customer/check_user',(req,res)=>{
        let email = req.body.email;
        check_user_customer.check_user(email,(found)=>{
          res.json(found);
        });
       });
      app.post('/upload',(req,res)=>{
        //console.log(req.body);
        let request = req;
        let response = res;
        let body = '';
      req.on('data', data => {
        body += data;
        console.log('Size: ', body.length / (1024 * 1024));
      });
        req.on('end',()=>{
          console.log('done');
          console.log(req.files.filename);
          res.end('ok');
          upload.upload(req,res);

        });
        //console.log(req.files.filename);
        // req.on('end',upload.upload(request,response,(found)=>{
        //  res.json(found);
        //  })
        // );
        
      });
      app.post('/photos/upload', uploads.array('photos', 12), (req, res, next)=> {
  // req.files is array of `photos` files
  // req.body will contain the text fields, if there were any

                    console.log(req.files[0].originalname);
                    console.log(req.body);
                    upload_grid.upload(req,res);
                    for(var i=0;i<req.files.length;i++)
                    upload_s3.s3(req,res,req.files[i]);
                  //res.end("success");

      });
      app.post('/get_user_id',(req,res)=>{
          id.create_id("US",req.body.city,req.body.pin,(found)=>{
            res.json(found);
            //console.log(found);
          });
      });

//new hotel route
      app.post('/hotel/register',uploads.array('photos', 12),(req,res)=>{
        //console.log(req.files[0].path);
        hotel.register(req,res,(found)=>{
            res.json(found);
        });
      });
       app.post('/partner/upload_image',uploads.array('photos', 12),(req,res)=>{
        //console.log(req.files[0].path);
        upload_partner_image.upload(req,res,(found)=>{
            res.json(found);
        });
      });
      app.post('/customer/upload_image',uploads.array('photos', 12),(req,res)=>{
        //console.log(req.files[0].path);
        upload_customer_image.upload(req,res,(found)=>{
            res.json(found);
        });
      });

      app.post('/hotel/update',uploads.array('photos', 12),(req,res)=>{
        console.log(req.files.length);
        hotel.update(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/hotel/delete',uploads.array('photos', 12),(req,res)=>{
        hotel.deleteHotel(req,res,(found)=>{
            res.json(found);
        });
      });

      app.post('/hotel/get',(req,res)=>{
        //console.log(req.body);
          hotel.get(req,res,(found)=>{
              res.json(found);
          });
      });
      app.post('/hotel/get/address',(req,res)=>{
        //console.log(req.body);
          hotel.getAddress(req,res,(found)=>{
              res.json(found);
          });
      });

      app.post('/hotel/getlist', (req, res)=>{
        hotel.getList(req, res, (found)=>{
            res.json(found);
        });
      });

//new room routes
      app.post('/room/get',(req,res)=>{
          room.get_room(req,res,(found)=>{
            res.json(found);
          });
      });

      app.post('/room/add',uploads.array('photos',12),(req,res)=>{
          room.add(req,res,(found)=>{
            res.json(found);
          });
      });

      app.post('/room/update',uploads.array('photos', 12),(req,res)=>{
          room.update(req,res,(found)=>{
            res.json(found);
          });
      });

      app.post('/room/delete',(req,res)=>{
        room.deleteRoom(req,res,(found)=>{
            res.json(found);
        });
      });

      app.post('/customer/order/create',(req,res)=>{ 
        customer_order.create(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/customer/order/status',(req,res)=>{ 
        customer_order.status(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/customer/order/status_callback',(req,res)=>{ 
        customer_order.status_callback(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/customer/order/list',(req,res)=>{ 
        customer_order.list(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/vendor/bookings',(req,res)=>{ 
        customer_order.booked_offers(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/release_offer',(req,res)=>{
        // let request = req;
        // let response = res;
        release_offer.release(req,res,(found)=>{
          res.json(found);
        });
      });
      
      app.post('/get_offers',(req,res)=>{
        // console.log(req.body);
        get_offers.get(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/get_credits',(req,res)=>{
        get_credits.get(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/get_credits_customer',(req,res)=>{
        get_credits.getCustomer(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/get_offer_by_owner',(req,res)=>{
        get_offer_by_owner.get_offer_by_owner(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/wish/create',(req,res)=>{
        wish.create(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/wish/get',(req,res)=>{
        wish.get(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/wish/delete',(req,res)=>{
        wish.delete(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/wish/vendor/get',(req,res)=>{
        wish.get_wishes_vendor(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/wish/vendor/reply',(req,res)=>{
        wish.vendor_reply(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/wish/vendor/getreplies',(req,res)=>{
        wish.get_vendor_replies(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/wish/replies',(req,res)=>{
        wish.get_wish_replies(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/wish/confirmreply',(req,res)=>{
        wish.confirm_reply(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/contactus/send',(req,res)=>{
        contactUs.create(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/contactus/get',(req,res)=>{
        contactUs.get(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/contactus/feedback/send',(req,res)=>{
        contactUs.create_feedback(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/contactus/feedback/get',(req,res)=>{
        contactUs.get_feedback(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/contactus/feedback/reply',(req,res)=>{
        contactUs.reply_feedback(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/create_order',(req,res)=>{
        order.create(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/get_order_status',(req,res)=>{
        order.get_status(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/list_orders',(req, res)=>{
        order.list_orders(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/add_points',(req,res)=>{
        points.add(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/add_points_customer',(req,res)=>{
        points.addCustomer(req,res,(found)=>{
          res.json(found);
        });
      });

      app.post('/get_offer_details',(req,res)=>{
        // console.log(req.body);
        get_offers.get_details(req,res,(found)=>{
          res.json(found);
        });
      });

      //to test send_notif util
      app.post('/gcm',(req,res)=>{
        // to : gcm token of the user (saved in usermodel on registration)

        send_notif.send(req.body.topic,req.body.message,req.body.to,req.body.metadata,(found)=>{
          res.json(found);
        });
      });
      app.post('/gcm_customer',(req,res)=>{
        // to : gcm token of the user (saved in usermodel on registration)

        customer_send_notif.send(req.body.topic,req.body.message,req.body.to,req.body.metadata,(found)=>{

          res.json(found);
        });
      });

      app.post('/schedule_notif',(req,res)=>{
        console.log(req.body);
        schedule_notif.schedule(req.body.date,req.body.topic,req.body.message,req.body.to,(found)=>{
            res.json(found);
        });

      });
      app.post('/feedback/submit',(req,res)=>{
        console.log(req.body);
        feedback.submit(req,res,(found)=>{
          res.json(found);
        });
      });
      app.post('/partner/check_credit',(req,res)=>{
        get_credits.check(req,res,(found)=>{
          res.json(found);
        })
      });
      app.post('/customer/check_credit',(req,res)=>{
        get_credits.checkCustomer(req,res,(found)=>{
          res.json(found);
        })
      });
      app.post('/offer/delete',(req,res)=>{
        get_offers.delete(req,res,(found)=>{
            res.json(found);
        });
      });

      app.post('/redeem_history',(req,res)=>{
        redeem_history.redeem(req,res,(found)=>{
            res.json(found);
        });
      });

      app.post('/hotel/booking/create',(req,res)=>{
        bookingHotel.createBooking(req,res,(found)=>{
            res.json(found);
        });
      });
    app.post('/get_city_list',(req,res)=>{
        get_city_list.by_service(req,res,(found)=>{
            res.json(found);
        });
    });


  }     