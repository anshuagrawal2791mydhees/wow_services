'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

var wishSchema = new Schema({
    wishId      : String,
    //wishTitle   : String,
    customerId  : String,
    customerName    : String,           //should not be included
    city        : String,
    area        :  String,
    noOfRooms   : Number,
    checkIn     : Date,
    checkOut    : Date,
    adults      : Number,
    children    : Number,
    seriousness : String,
    category    : String,
    range:  {                           //Range should be per room
        start   : { type: Number, default: 0 },
        end     : { type: Number, default: 0 }, 
    },
    replyId     : String,
    negotiatedPrice     : Number,       //per room
    preferredHotelIds   : [String],
    expiresAt   : Date,
    createdBy   : String,
    createdAt   : {
                    type: Date,
                    default: Date.now
                },
    modifiedAt  : {
                    type: Date,
                    default: Date.now
                },
    isConfirmed : {   
                    type: Boolean,
                    default: false
                },
    isDeleted   : {   
                    type: Boolean,
                    default: false
                }
});

var wish = mongoose.model('wishes',wishSchema);
module.exports = wish;
