'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var bookingHotelSchema = new Schema({
	bookingId 		: String,		//as created for juspay
	customerId 		: String,
	bookingName 	: String,
	bookingContact 	: String,
	bookingEmail 	: String,
	bookingType		: String,
	offerId 		: String,
	wishId 			: String,
	checkIn 		: Date,
	checkOut 		: Date,
	ownerId 		: String,
	hotelId 		: String,
	hotelName		: String,
	roomId 			: String,
	roomType		: String,
	noOfRooms 		: Number,
	adults 			: Number,
	children 		: Number,
	bookingDetails  : [{ name : String, sex : String, age : Number }],
	status 			: Number,		//2 for paid, 0 for cancelled, 1 for pending
	orderId 		: String,
	rate 			: Number, 		//rate per room per night
	amount 			: Number,
	discountId 		: String,
	discountAmount 	: Number,
	currency 		: String,
	confirmed 		: { type: Boolean, default: false },	//from hotelier
	confirmedAt 	: { type: Date },
	createdBy		: String,
	createdAt 		: {	type: Date,	default: Date.now },
	modifiedBy		: String,
	modifiedAt		: { type: Date,	default: Date.now }
});

var bookingHotel = mongoose.model('bookingHotel',bookingHotelSchema);
module.exports = bookingHotel;
