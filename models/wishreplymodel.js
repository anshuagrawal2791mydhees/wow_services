'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

var wishReplySchema = new Schema({
    replyId     : String,
    wishId      : String,
    ownerId     : String,
    hotelId     : String,
    hotelName   : String,
    hotelAddress  : String,
    roomId      : String,
    roomType    : String,
    price       : Number,
    comment     : String,
    createdAt   : {
                    type: Date,
                    default: Date.now
                }
});
//no option to delete reply

var wishReply = mongoose.model('wishes_reply',wishReplySchema);
module.exports = wishReply;
