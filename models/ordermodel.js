'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

var orderSchema = new Schema({
	order_id : String,		//as created for juspay
	customer_id : String,
	orderType : String,		//1 for offer, 2 for wish
	booking : {
		name : String,
		contact : String,
		checkIn : Date,
		checkOut : Date,
		confirmed : { type: Boolean, default: false },
		confirmedAt : { type: Date },
		hotelName	: String,
		roomType	: String,
		noOfRooms 	: Number,
		offerId 	: { type: String, default: null },
		offerName 	: { type: String, default: null },
		wishId 		: { type: String, default: null },
		replyId 	: { type: String, default: null }	
	},
	ownerId : String,		//for easy linking to corresponding owner
	offerId : String,
	hotelId : String,
	roomId : String,
	status : String,		//for easy linking to corresponding owner
	offerName : String,		//failed during generation or during payment
	statusId : Number,		//0 failed, 1 confirmed, 2 pending
	discountId : String,	//discount model for future use
	discountAmount : Number,
	amount : Number,
	currency : String,
	refunded : {   
                type: Boolean,
                default: false
           	 	},		
            	//check order status for the same
	amountRefunded : {
				type: Number,
				default: 0
				},	//future use
	createdBy: String,
	createdAt : {
	            	type: Date,
	            	default: Date.now
	            },
	modifiedBy: String,
	modifiedAt: {
	            	type: Date,
	            	default: Date.now
	            }

});

var order = mongoose.model('orders',orderSchema);
module.exports = order;

/*
payment status of Juspay
NEW 			10	Newly created order
PENDING_VBV		23	Authentication is in progress
CHARGED			21	Successful transaction
AUTHENTICATION_FAILED	26	User did not complete authentication
AUTHORIZATION_FAILED	27	User completed authentication. But bank refused the transaction
*/