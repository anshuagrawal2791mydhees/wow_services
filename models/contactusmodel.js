'use strict';
var mongoose=require('mongoose');
var Schema =  mongoose.Schema;
var contactUsSchema=new Schema({
    messageId   : String,
    customerId  : String,
    customerName : String,
    email       : String,
    ownerId     : String,
    message     : String,
    createdAt   : {
        type    : Date,
        default : Date.now
    },
    reply       : String,
    replyTime   : Date,
    type        : Number,       // 1 for contact us, 2 for feedback
    status      :  Number       //1 for new, 2 for replied

});

var contactUs=mongoose.model('contactUs',contactUsSchema);
module.exports=contactUs;