'use strict';
const mongoose = require('mongoose');  
const configs = require('../config/config');
const rand = require('csprng');

const Schema = mongoose.Schema;
var referralCode = rand(24,24);

let userSchema = mongoose.Schema({
	id : String,        
     email: String,  
     hashed_password: String,
     gcm_token: String,    
     salt : String,  
     name:String,
     phone:String,
     city:String,
     temp_str:String ,
     address_line1:String,
     address_line2:String,
     imagesS3 : {
        name: {type:[String]},
        description: {type:[String]}

    },
     address:String,
     pin:Number,
     location: {type:[Number],index : '2d'},
     services: [],
     verified:Boolean,
     created_by:String,
     created_at:Date,
     modified_by:String,
     modified_at:Date,
     activity_status: Boolean,
     last_login:Date,
     promotionalPoints:{type:Number,default:0},
     purchasedPoints:{type:Number,default:0},
     totalPoints:{type:Number,default:0},
     referralCode:{type:String,default:referralCode.toUpperCase()}

});  

userSchema.pre('save',function(next){
	let currentDate = new Date();
	this.modified_at = currentDate;
     this.totalPoints = this.promotionalPoints+this.purchasedPoints;
	next();
});


module.exports= mongoose.model('Hotel Partner Users',userSchema);
