'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var offerSchema = new Schema({
	hotelImagesS3:{
		name: String,
		url:String
	},
	starHotel: Number,
	hotelName: String,
	address_line1: String,
	address_line2: String,
	address: String,
	checkIn: Date,
	checkOut: Date,
	scheduledAt: Date,
	expiresAt: Date,
	priceMRP: Number,
	wowPrice: Number
});