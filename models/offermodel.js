'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

var offerSchema = new Schema({
ownerId: String,
hotelId : String,
hotelName: String,
offerId: String,
offerName: String,
roomId: String,
roomType: String,           //same as for the roomId
noOfRooms: Number,
bookedRooms: Number,
priceMRP: Number,
wowPrice: Number,
checkIn: Date,
checkOut: Date,
scheduledAt: Date,
expiresAt: Date,
createdBy: String,
createdAt : {
                type: Date,
                default: Date.now
            },
modifiedBy: String,
modifiedAt: {
                type: Date,
                default: Date.now
            },
isVerified: {   
                type: Boolean,
                default: false
            },
status:     {   
                type: Boolean,
                default: false
            },
isDeleted:   {   
                type: Boolean,
                default: false
            }
});

var offer = mongoose.model('offers',offerSchema);
module.exports = offer;