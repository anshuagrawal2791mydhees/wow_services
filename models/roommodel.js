'use strict';

var mongoose=require('mongoose');

var Schema =  mongoose.Schema;

var roomsSchema=new Schema({
    ownerId:String,
    roomId:String,
    hotelId: String,
    hotelName : String,
    roomType : String,
    noOfRooms : Number,
    priceMRP: Number,
    salePrice: Number,
    wowPrice: Number,
    ac:Boolean,
    adults: Number,
    children:Number,
    facilities: {
                hasWifi : Boolean,
                hasDining   : Boolean,
                hasTv   :   Boolean,
                hasGym  :   Boolean,
                hasBusinessService  :   Boolean,
                hasCoffeeShop  :   Boolean,
                hasFrontDesk  :   Boolean,
                hasSpa  :   Boolean,
                hasLaundry  :   Boolean,
                hasOutDoorgames  :   Boolean,
                hasParking  :   Boolean,
                hasRoomService  :   Boolean,
                hasSwimming  :   Boolean,
                hasTravelAssistance  :   Boolean             
                },
    imagesS3 : {
        name: [String],
        description: [String]

    },
    imagesGridFS:{
        name: [String],
        description: [String]
    },
    createdBy: String,
    createdAt : {
                type: Date,
                default: Date.now
                },
    modifiedBy: String,
    modifiedAt: {
                type: Date,
                default: null
                },
    isVerified: {   
                type: Boolean,
                default: false
                },
    
    status:     {   
                type: Boolean,
                default: false
                },
    isDeleted:  {
                type: Boolean,
                default: false
                },
    lastActivity : {
                type: Date,
                default: null
                }
    
});

var rooms=mongoose.model('rooms',roomsSchema);

module.exports=rooms;