'use strict';

var mongoose=require('mongoose');

var Schema =  mongoose.Schema;

var hotelSchema=new Schema({
    hotelId:String,
    ownerId:String,
    hotelName : String,
    hotelType : String,
    noOfRooms : Number,
    phoneNumber: String,
    mobileNumber: String,
    address_line1:String,
    address_line2:String,
    address: String,
    location: {type:[Number],index : '2d'},
    city: String,
    pin: Number,
    facilities: {
                hasWifi : Boolean,
                hasDining   : Boolean,
                hasTv   :   Boolean,
                hasGym  :   Boolean,
                hasBusinessService  :   Boolean,
                hasCoffeeShop  :   Boolean,
                hasFrontDesk  :   Boolean,
                hasSpa  :   Boolean,
                hasLaundry  :   Boolean,
                hasOutDoorgames  :   Boolean,
                hasParking  :   Boolean,
                hasRoomService  :   Boolean,
                hasSwimming  :   Boolean,
                hasTravelAssistance  :   Boolean             
                },
    userRatings:{
        frontOfficeStaff:{
                type: Number,
                default: 0
                },
        restaurantAmbiance:{
                type: Number,
                default: 0
                },
        restaurantStaff:{
                type: Number,
                default: 0
                },
        houseKeeping:{
                type: Number,
                default: 0
                },
        cleanliness:{
                type: Number,
                default: 0
                },
        checkInProcedure:{
                type: Number,
                default: 0
                },
        likelyToStayAgain:{
                type: Number,
                default: 0
                },
        overall:{
                type: Number,
                default: 0
                }
    },
    numberOfReviews:{
                type: Number,
                default: 0
                },
    reviews:[{
        review:String,
        overallRating:Number, //bookingId
        customerId:String,
        customerName:String,
        roomId:String,
        roomType:String
    }],
    starHotel: Number,
    policies : [String],
    imagesS3 : {
        name: [String],
        description: [String]

    },
    imagesGridFS:{
        name: [String],
        description: [String]
    },
    createdBy: String,
    createdAt : {
                type: Date,
                default: Date.now
                },
    modifiedBy: String,
    modifiedAt: {
                type: Date,
                default: null
                },
    isVerified: {   
                type: Boolean,
                default: false
                },
    
    status:     {   
                type: Boolean,
                default: false
                },
    isDeleted:  {   
                type: Boolean,
                default: false
                },
    lastActivity : {
                type: Date,
                default: null
                }
    
});

var hotel=mongoose.model('hotel',hotelSchema);

module.exports=hotel;