'use strict';

const express = require('express');
const connect = require('connect');
const app = express();
const logger = require('morgan');
const bodyParser = require('body-parser');
const multer  = require('multer');
const mongoose = require('mongoose');  
const configs = require('./config/config');


app.set('port',process.env.PORT ||3000);

//configuration
app.use(express.static(__dirname + 'public'));
//app.use(multer({ dest: './uploads/'}));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
      extended: true
  }));
require('./routes/routes.js')(app);  
//routes
// require()

app.listen(app.get('port'),()=>{
	mongoose.connect('mongodb://'+configs.username+':'+configs.password+configs.db_url);

	console.log("now running on port: "+app.get('port'));
});
